const flashData = $('.flash-data').data('flashdata');

    if (flashData) {
        Swal({
           title: 'Data Pegawai ',
            text: 'Berhasil ' + flashData,
            type: 'success'
        });
    }

    
    //tombol hapus
    $('.tobol-hapus').on('click', function () {
       
        e.preventDefault();
        const href = $(this).attr('href');
        
        Swal.fire({
            title: 'Apakah anda yakin',
            text: "Data pegawai akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus Data!'
        }).then((result) => {
          if (result.value) {
              document.location.href = href;
          }
        })
        
    });