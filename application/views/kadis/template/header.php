<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title><?= $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Aplikasi Pengelolaan SPPD Berbasis Web" name="description" />
        <meta content="SPPD" name="Teguh Dwi Prasetyo" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url('assets/vendor/hyper/'); ?>assets/images/favicon.ico">
    
        <!-- third party css -->
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/vendor/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/vendor/select.bootstrap4.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/vendor/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- third party css end -->

        <!-- App css -->
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Begin page -->
        <div class="wrapper">