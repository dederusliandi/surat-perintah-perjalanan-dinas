<div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="<?= site_url('kadis/sppdperintah') ?>" class="btn btn-success">Kembali</a><br><br>
                                <form action="<?= site_url("kadis/updateStatusPerintah/".$sppd->id_surat) ?>" method="POST">
                                    <table id="basic-datatable" class="table table-bordered table-sm dt-responsive nowrap">
                                        <tr>
                                            <td>NO Surat</td>
                                            <td>:</td>
                                            <td><?= $sppd->nosurat ?></td>
                                        </tr>

                                        <tr>
                                            <td>Tanggal Surat</td>
                                            <td>:</td>
                                            <td><?= $sppd->tglsurat ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>
                                            <?php if ($sppd->statussurat == "1") : ?>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-warning">On Proses</span>
                                                </div>

                                                <?php elseif ($sppd->statussurat == "2") : ?>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-danger">Di Tolak</span>
                                                </div>

                                                <?php elseif ($sppd->statussurat == "3") : ?>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-success">Di Terima</span>
                                                </div>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>:</td>
                                            <td>
                                            <select name="statussurat" id="" class="form-control" <?= $sppd->statussurat == "3" ? "readonly" : "" ?>>
                                                    <?php
                                                        if($sppd->statussurat != 3) {
                                                            ?>
                                                                <option value="1" <?= $sppd->statussurat == "1" ? "selected" : "" ?>>On Process</option>
                                                                <option value="2" <?= $sppd->statussurat == "2" ? "selected" : "" ?>>Ditolak</option>
                                                            <?php
                                                        }
                                                    ?>
                                                    <option value="3" <?= $sppd->statussurat == "3" ? "selected" : "" ?>>Diterima</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <button class="btn btn-primary" type="submit">submit</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>