<!DOCTYPE html>
<html>
<head>
	<title>Input Kepala Dinas</title>
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logopemkabtasik.png">

    <!-- third party css -->
    <link href="<?php echo base_url().'assets/custom-bootstrap/superadmin/css/vendor/jquery-jvectormap-1.2.2.css' ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/app.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
    
<!-- Begin page -->
        <div class="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!-- LOGO -->
                    <a href="#" class="logo text-center">
                        <span class="logo-lg">
                            <img src="<?php echo base_url().'assets/img/logopemkabtasik.png' ?>" alt="" height="50">
                        </span>
                        <h4 style="color: white; margin-top: -5px;"> SPPD TASIKMALAYA </h4>
                        <span class="logo-sm">
                            <img src="assets/images/logo_sm.png" alt="" height="16">
                        </span>
                    </a>
                    
                    <hr>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav">

                        <li class="side-nav-title side-nav-item">Navigation</li>

                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'c_admin'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account"></i>
                                <span> Data Petugas Dinas </span>
                            </a>
                        </li>
                        
                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'c_admin/dataPegawai'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account-box"></i>
                                <span> Data Pegawai </span>
                            </a>
                        </li>
                        
                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'c_admin/dataKadis'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account-circle"></i>
                                <span> Data Kepala Dinas </span>
                            </a>
                        </li>
            
                    </ul>
                    
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Topbar Start -->
                    <div class="navbar-custom">
                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                    aria-expanded="false">
                                    <span class="account-user-avatar"> 
                                        <img src="<?php echo base_url().'assets/img/logopemkabtasik.png' ?>" alt="user-image" class="rounded-circle">
                                    </span>
                                    <span>
                                        <B>Super Admin</B>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="mdi mdi-account-circle"></i>
                                        <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="<?php echo base_url().'login/logout'?>" class="dropdown-item notify-item">
                                        <i class="mdi mdi-logout"></i>
                                        <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>
                        <button class="button-menu-mobile open-left disable-btn">
                            <i class="mdi mdi-menu"></i>
                        </button>
                        <div class="app-search">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="mdi mdi-magnify"></span>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end Topbar -->
                    
                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Input Kepala Dinas</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col">
                                <div class="card widget-flat">
                                    <div class="card-body">
                                        <?php echo $alert;?>
                                        <form class="form-horizontal" method="post">
                                            <div class="form-group row mb-3">
                                                <label for="inputNAMA" class="col-3 col-form-label">Nama</label>
                                                <div class="col-9">
                                                    <input type="text" name="nama" value="<?php echo $satu['nama'];?>" class="form-control" id="inputNAMA" placeholder="Nama">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label for="inputNIP" class="col-3 col-form-label">NIP</label>
                                                <div class="col-9">
                                                    <input type="number" name="nip" value="<?php echo $satu['nip'];?>" class="form-control" id="inputNIP" placeholder="Nomor Induk Pegawai">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label for="inputUsername" class="col-3 col-form-label">Username</label>
                                                <div class="col-9">
                                                    <input type="text" name="username" value="<?php echo $satu['username'];?>" class="form-control" id="inputUsername" placeholder="Username">
                                                </div>
                                            </div>
                                            <div class="form-group row mb-3">
                                                <label for="inputPassword" class="col-3 col-form-label">Password</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        <input type="text" name="password" value="<?php echo $satu['password'];?>" class="form-control" id="inputPassword" placeholder="Password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 justify-content-end row">
                                                <input type="hidden" name="level" value="1">
                                                <input type="hidden" name="id" value="<?php echo $satu['id'];?>">
                                                <div class="col-9">
                                                    <button type="submit" name="simpan" class="btn btn-info">Simpan</button>
                                                    <a href="<?php echo site_url('c_admin/dataKadis');?>" class="btn btn-danger">Kembali</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- container -->

                </div>
                <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2018 © Hyper - Coderthemes.com
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-md-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- App js -->
        <script src="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/js/app.min.js"></script>

        <!-- third party js -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/jquery.dataTables.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.bootstrap4.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.responsive.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/responsive.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.buttons.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.html5.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.flash.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.print.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.keyTable.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.select.min.js' ?>"></script>
        <!-- third party js ends -->

        <!-- demo app -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/pages/demo.datatable-init.js' ?>"></script>
        <!-- end demo js-->

</body>
</html>