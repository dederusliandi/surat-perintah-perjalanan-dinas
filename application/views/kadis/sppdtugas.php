                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top: 10px">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <table id="basic-datatable" class="table table-bordered table-sm dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Surat</th>
                                                <th>Tanggal Surat</th>
                                                <th>Biaya</th>
                                                <th>Status</th>
                                                <th style="text-align: center;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $n = 0;
                                            foreach($sppd as $d){ 
                                                $n++;?>
                                                <tr>
                                                    <td><?php echo $n;?></td>
                                                    <td><strong><?php echo $d->nosurat;?></strong></td>
                                                    <td><?php echo shortdate_indo($d->tglsurat);?></td>
                                                    <td>
                                                    <?php
                                                        @$check = $this->db->where('id_surat', $d->id_surat)->get('biaya_sppd')->result()[0];
                                                    ?>
                                                    <?php
                                                        if(!$check) {
                                                            echo "-";
                                                        } else {
                                                           if($check->status == 0) {
                                                            echo "<a href='".site_url('kadis/accBiayaTugas/'.$d->id_surat)."' class='btn btn-primary'>Acc Biaya</a>";
                                                            echo " <a href='".site_url("kadis/tolakBiayaTugas/".$d->id_surat)."' class='btn btn-danger'>Tolak Biaya</a>";
                                                           }  else {
                                                               echo "acc";
                                                           }
                                                        }
                                                    ?>
                                                </td>
                                                    <td>
                                                        <?php if ($d->statussurat == "1") : ?>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <span class="badge badge-warning">On Proses</span>
                                                        </div>
                                                        <?php elseif ($d->statussurat == "2") : ?>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <span class="badge badge-danger">Di Tolak</span>
                                                        </div>
                                                        <?php elseif ($d->statussurat == "3") : ?>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <span class="badge badge-success">Di Terima</span>
                                                        </div>
                                                        <?php endif ?>
                                                    </td>
                                                    <td>
                                                        <a href="<?= site_url('kadis/detailSuratTugas/'.$d->id_surat); ?>" class="action-icon" data-toggle="tooltip" data-placement="bottom" title="Detail"> 
                                                            <i class="mdi mdi-eye"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->