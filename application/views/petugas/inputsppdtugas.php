                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">SPPD</a></li>
                                            <li class="breadcrumb-item"><a href="#">SPPD Tugas</a></li>
                                            <li class="breadcrumb-item active">Tambah SPPD</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Tambah SPPD Tugas</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top: -10px">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <form class="form-horizontal" method="post">
                                        <div class="form-group row mb-2">
                                            <label for="inputNoSurat" class="col-md-2 col-form-label">Nomor Surat</label>
                                                <div class="col-3">
                                                    <input type="text" name="nosurat" class="form-control" id="nosurat" value="<?= $kodeunik; ?>" style="cursor: not-allowed" readonly>
                                                </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label for="inputDasar" class="col-md-2 col-form-label">Dasar</label>
                                                <div class="col-10">
                                                    <input type="text" name="dasar" class="form-control" id="inputDasar" placeholder="Dasar" value="<?php echo $tugas['dasar']; ?>" required>
                                                </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label for="inputNama" class="col-md-2 col-form-label">Nama</label>
                                            <div class="col-10">
                                           
                                            <select class="js-example-basic-multiple" name="id_pegawai[]" multiple="multiple">
                                                <?php
                                                    $idPegawai =  [];
                                                    foreach($pegawai->result() as $row => $p) {
                                                        foreach($data_pegawai as $pegawai_selected) {
                                                            if($p->id_pegawai == $pegawai_selected->id_pegawai) {
                                                               $idPegawai[] = $pegawai_selected->id_pegawai;
                                                            }
                                                        }
                                                    }

                                                    foreach($pegawai->result() as $row => $p) {
                                                        ?>
                                                            <option value="<?= $p->id_pegawai ?>" <?= in_array($p->id_pegawai, $idPegawai) ? "selected" : "" ?>><?= $p->nip .' - '. $p->nama ?></option>
                                                        <?php
                                                    }
                                                ?>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-2">
                                            <label for="inputUntuk" class="col-md-2 col-form-label">Untuk</label>
                                                <div class="col-10">
                                                                                                        
                                                <input type="text" class="form-control tags" value="<?= $tugas['untuk']; ?>" id="untuk" name="untuk"><br><br>
                                                    
                                            <input type="hidden" name="id_surat" value="<?php echo $tugas['id_surat']; ?>">
                                            <input type="hidden" name="statussurat" value="1">
                                            <input type="hidden" name="jenissurat" value="2">
                                            <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                                            <a href="<?= site_url('petugas/sppdtugas'); ?>" class="btn btn-danger">Kembali</a>
                                                </div>
                                        </div>
                                    </form>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->