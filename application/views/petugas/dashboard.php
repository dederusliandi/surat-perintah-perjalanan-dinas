                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <!--<a href="<?//= base_url('administrator/forminputpegawai'); ?>" class="btn btn-sm btn-primary" style="margin-top: 25px">Tambah Akun</a>-->
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="card bg-primary">
                                    <div class="card-body">
                                        <h4 class="header-title text-white mb-4" style="text-align: center">
                                            SPPD Perintah
                                        </h4>
                                        <div class="text-center">
                                            <h1 class="font-weight-large text-white mb-2" style="margin-top: -15px">
                                                <?= $hitungperintah; ?>
                                            </h1>
                                            <p class="text-light text-uppercase font-13 font-weight-bold">Surat</p>
                                            <a href="<?= base_url('petugas/sppdperintah'); ?>" class="btn btn-outline-light btn-sm mb-1">Lihat
                                                <i class="mdi mdi-arrow-right ml-1"></i>
                                            </a>

                                        </div> <!-- end card-body-->
                                    </div> <!-- end card-->
                                </div>
                            </div> <!-- end col-->
                            <div class="col-sm-3">
                                <div class="card bg-primary">
                                    <div class="card-body">
                                        <h4 class="header-title text-white mb-4" style="text-align: center">
                                            SPPD Tugas
                                        </h4>
                                        <div class="text-center">
                                            
                                            <h1 class="font-weight-large text-white mb-2" style="margin-top: -15px">
                                                <?= $hitungtugas; ?>
                                            </h1>
                                            <p class="text-light text-uppercase font-13 font-weight-bold">Surat</p>
                                            <a href="<?= base_url('petugas/sppdtugas'); ?>" class="btn btn-outline-light btn-sm mb-1">Lihat
                                                <i class="mdi mdi-arrow-right ml-1"></i>
                                            </a>

                                        </div> <!-- end card-body-->
                                    </div> <!-- end card-->
                                </div>
                            </div>
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->