<!DOCTYPE html>
<html>
  <head>
    <title>Halaman Pegawai SPPD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logopemkabtasik.png">

    <!-- third party css -->
    <link href="<?php echo base_url().'assets/custom-bootstrap/superadmin/css/vendor/jquery-jvectormap-1.2.2.css' ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
      
    <!-- Custom Style -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/petugas/css/style.css" rel="stylesheet">

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/app.min.css" rel="stylesheet" type="text/css" />

  </head>
  <body>
      
        <!-- Begin page -->
        <div class="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu" style="background: #2d89ef">

                <div class="slimscroll-menu">

                    <!-- LOGO -->
                    <a href="#" class="logo text-center">
                        <span class="logo-lg">
                            <img src="<?php echo base_url(); ?>assets/img/logopemkabtasik.png" alt="" height="50">
                        </span>
                        <h4 style="color: white; margin-top: -5px;"> SPPD TASIKMALAYA </h4>
                        <span class="logo-sm">
                            <img src="assets/images/logo_sm.png" alt="" height="16">
                        </span>
                    </a>
                    
                    <hr>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav">

                        <li class="side-nav-title side-nav-item" style="color: #b8d7fa">Main Menu</li>

                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'petugas'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account-box"></i>
                                <span> List SPPD </span>
                            </a>
                        </li>
            
                    </ul>
                    
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Topbar Start -->
                    <div class="navbar-custom">
                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                    aria-expanded="false">
                                    <span class="account-user-avatar"> 
                                        <img src="assets/img/logopemkabtasik.png" alt="user-image" class="rounded-circle">
                                    </span>
                                    <span style="vertical-align: middle">
                                        <B>Pegawai</B>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="mdi mdi-account-circle"></i>
                                        <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="<?php echo base_url().'login/logout'?>" class="dropdown-item notify-item">
                                        <i class="mdi mdi-logout"></i>
                                        <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>
                        <button class="button-menu-mobile open-left disable-btn">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </div>
                    <!-- end Topbar -->
                    
                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data SPPD Perintah dan Tugas</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col">
                                <div class="card widget-flat">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs nav-justified nav-bordered mb-3">
                                            <li class="nav-item">
                                                <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                    <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                                                    <span class="d-none d-lg-block">Perintah</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#profile-b2" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                    <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                                                    <span class="d-none d-lg-block">Tugas</span>
                                                </a>
                                            </li>
                                            <!--<li class="nav-item">
                                                <a href="#settings-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
                                                    <span class="d-none d-lg-block">Settings</span>
                                                </a>
                                            </li>-->
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane show active" id="home-b2">
    <table id="basic-datatable" class="table table-bordered table-sm dt-responsive nowrap">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Lengkap</th>
          <th>NIP</th>
          <th>No. Surat</th>
          <th>Tanggal Surat</th>
          <th>Dasar</th>
          <th>Untuk</th>
          <th>Jabatan</th>
          <th>Golongan</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=0;
          foreach($perintah->result_array() as $d){ 
        $n++;?>
        <tr>
          <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
          <td style="vertical-align: middle;"><?php echo $d['nama'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nip'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nosurat'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglsurat'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['dasar'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['untuk'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['jabatan'];?> Hari</td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['golongan'];?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
                                            </div>
                                            <div class="tab-pane" id="profile-b2">
                                            <div class="table-responsive">
    <table id="selection-datatable" class="table table-bordered table-sm dt-responsive nowrap" width="100%">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Lengkap</th>
          <th>NIP</th>
          <th>No. Surat</th>
          <th>Tanggal Surat</th>
          <th>Dasar</th>
          <th>Untuk</th>
          <th>Jabatan</th>
          <th>Golongan</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=0;
        foreach($tugas->result_array() as $d){ 
        $n++;?>
        <tr>
          <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
          <td style="vertical-align: middle;"><?php echo $d['nama'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nip'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nosurat'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglsurat'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['dasar'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['untuk'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['jabatan'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['golongan'];?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
                                            </div>
                                            </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- container -->

                </div>
                <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2018 © Hyper - Coderthemes.com
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-md-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- App js -->
        <script src="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/js/app.min.js"></script>

        <!-- third party js -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/jquery.dataTables.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.bootstrap4.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.responsive.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/responsive.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.buttons.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.html5.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.flash.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.print.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.keyTable.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.select.min.js' ?>"></script>
        <!-- third party js ends -->

        <!-- demo app -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/pages/demo.datatable-init.js' ?>"></script>
        <!-- end demo js-->

  </body>
</html>
