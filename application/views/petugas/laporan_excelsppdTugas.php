<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<link href="<?php echo base_url().'assets/bootstrap2/css/bootstrap.css'?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/bootstrap2/css/bootstrap.min.css'?>" rel="stylesheet">



<table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Lengkap</th>
          <th>NIP</th>
          <th>No. Surat</th>
          <th>Tanggal Surat</th>
          <th>Keperluan</th>
          <th>Tujuan</th>
          <th>Lama Hari</th>
          <th>Tanggal Berangkat</th>
          <th>Tanggal Kembali</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=0;
        foreach($semua->result_array() as $d){ 
        $n++;?>
        <tr>
          <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
          <td style="vertical-align: middle;"><?php echo $d['nama'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nip'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nosurat'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglsurat'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['keperluan'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['tujuan'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['lamahari'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglberangkat'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglkembali'];?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>