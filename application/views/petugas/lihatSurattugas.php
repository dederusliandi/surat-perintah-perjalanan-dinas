<html>
    <head>
        <title>Print Surat Perintah</title>
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/css/stylesuratperintah.css">
    </head>
<body onclick="myFunction()">
    <img src="<?php echo base_url(); ?>assets/img/logopemkabtasik.png" width="100px" height="100px">
    <h4 align="center">PEMERINTAH DAERAH KABUPATEN TASIKMALAYA</h4>
    <p align="center"><B>NAMA PERANGKAT DAERAH</B></p>
    <h5 align="center">NAMA UPT</h5><br>
    <hr size="2" color="black">
    <hr size="1" color="black" style="margin-top: -5px">
    
    <!-- atas -->
    
    <div class="isiatas">
        <h4>SURAT TUGAS</h4>
        <p><B>NOMOR <?php echo $data_tugas['nosurat']; ?></B></p>
    </div>

    
    <div class="isidasar">
        <table>
            <tr>
                <td style="padding-right: 50px">Dasar</td>
                <td style="padding-right: 50px">:</td>
                <td><?php echo $data_tugas['dasar']; ?></td>
            </tr>
        </table><br>
    </div>
    
    <div class="isimemerintahkan">
        <h4>MEMERINTAHKAN:</h4>
        <table>
            <tr>
                <td style="padding-right: 40px">Kepada</td>
                <td style="padding-right: 50px">:</td>
                <td style="padding-right: 20px">1.</td>
                <td style="padding-right: 20px">Nama</td>
                <td style="padding-right: 20px">:</td>
                <td><?php echo $data_tugas['nama']; ?></td>
            </tr>
            <tr>
                <td style="padding-right: 40px"></td>
                <td style="padding-right: 50px"></td>
                <td style="padding-right: 20px"></td>
                <td style="padding-right: 20px">Pangkat/gol</td>
                <td style="padding-right: 20px">:</td>
                <td><?php echo $data_tugas['golongan']; ?></td>
            </tr>
            <tr>
                <td style="padding-right: 40px"></td>
                <td style="padding-right: 50px"></td>
                <td style="padding-right: 20px"></td>
                <td style="padding-right: 20px">NIP</td>
                <td style="padding-right: 20px">:</td>
                <td><?php echo $data_tugas['nip']; ?></td>
            </tr>
            <tr>
                <td style="padding-right: 40px"></td>
                <td style="padding-right: 50px"></td>
                <td style="padding-right: 20px"></td>
                <td style="padding-right: 20px">Jabatan</td>
                <td style="padding-right: 20px">:</td>
                <td><?php echo $data_tugas['jabatan']; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="isiuntuk">
        <table>
            <tr>
                <td style="padding-right: 50px">Untuk</td>
                <td style="padding-right: 50px">:</td>
                <td style="padding-right: 20px">1.</td>
                <td><?php echo $data_tugas['untuk']; ?></td>
            </tr>
            <!--<tr>
                <td style="padding-right: 40px"></td>
                <td style="padding-right: 50px"></td>
                <td style="padding-right: 20px">2.</td>
                <td>.................</td>
            </tr>
            <tr>
                <td style="padding-right: 40px"></td>
                <td style="padding-right: 50px"></td>
                <td style="padding-right: 20px">3.</td>
                <td>.................</td>
            </tr>-->
        </table>
    </div>
    
    <!-- end atas -->
    
    <!-- sign -->
    
    <div id="bawah">
        <h4>Bandung, 20 Desember 2018</h4>
        <center><p><B>JABATAN</B></p></center>
        <h5>NAMA</h5>
    </div>
    
    <!-- end sign -->
    
    <script>
        function myFunction() {
            window.print();
        }
    </script>
    
</body>
</html>