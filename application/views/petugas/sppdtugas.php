                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">SPPD</a></li>
                                            <li class="breadcrumb-item"><a href="#">SPPD Perintah</a></li>
                                            <li class="breadcrumb-item active">Details SPPD</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Details SPPD Perintah</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top: -10px">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <table id="basic-datatable" class="table table-bordered table-sm dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Surat</th>
                                                <th style="text-align: center;">Tanggal Surat</th>
                                                <th style="text-align: center;">Status</th>
                                                <th style="text-align: center;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $n=0;
                                          foreach($tugas as $d){ 
                                        $n++;?>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
                                                <td style="vertical-align: middle;"><strong><?php echo $d->nosurat ?></strong></td>
<!--
                                                <td style="vertical-align: middle;"><?php //echo $d['nip'];?></td>
                                                <td style="vertical-align: middle;"><?php //echo $d['nama'];?></td>
-->
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php echo shortdate_indo($d->tglsurat);?>
                                                </td>
                                                <td style="text-align: center; vertical-align: middle;">

                                                <?php if ($d->statussurat == "1") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-warning">On Proses</span>
                                                </div>

                                                <?php elseif ($d->statussurat == "2") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-danger">Di Tolak</span>
                                                </div>

                                                <?php elseif ($d->statussurat == "3") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-success">Di Terima</span>
                                                </div>

                                                <?php elseif ($d->statussurat == "4") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-success">Bisa Di Print</span>
                                                </div>

                                                <?php endif ?>

                                                </td>
                                                <td style="vertical-align: middle" align="center">

                                                <?php if ($d->statussurat == "1") : ?>

<!--
                                                <a href="<?//= site_url('petugas/detailsuratperintah/'.$d->id_surat); ?>" class="action-icon" data-toggle="tooltip" data-placement="bottom" title="Detail"> 
                                                    <i class="mdi mdi-eye"></i>
                                                </a>
-->
                                                <a href="<?= site_url('petugas/inputsppdperintah/'.$d->id_surat); ?>" class="action-icon"data-toggle="tooltip" data-placement="bottom" title="Edit"> 
                                                    <i class="mdi mdi-square-edit-outline"></i>
                                                </a>
                                                <a href="<?= site_url('kadis/hapussppd/'.$d->id_surat); ?>" class="action-icon"data-toggle="tooltip" data-placement="bottom" title="Hapus"> 
                                                    <i class="mdi mdi-delete"></i>
                                                </a>

                                                <?php elseif ($d->statussurat == "2") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <span class="badge badge-light">No Action</span>
                                                </div>

                                                <?php elseif ($d->statussurat == "3") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="<?php echo site_url('petugas/printsurattugas/'.$d->id_surat);?>" class="btn btn-sm btn-danger"><i class="mdi mdi-printer"></i></a>
                                                </div>

                                                <?php elseif ($d->statussurat == "4") : ?>

                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="<?php echo site_url('petugas/printsurattugas/'.$d->id_surat);?>" target="_blank" class="btn btn-sm btn-primary"><i class="mdi mdi-printer"></i></a>
                                                </div>

                                                <?php endif ?>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->