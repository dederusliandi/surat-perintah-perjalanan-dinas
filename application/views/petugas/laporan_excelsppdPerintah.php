<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<link href="<?php echo base_url().'assets/bootstrap2/css/bootstrap.css'?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/bootstrap2/css/bootstrap.min.css'?>" rel="stylesheet">



<table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Lengkap</th>
          <th>NIP</th>
          <th>No. Surat</th>
          <th>Tanggal Surat</th>
          <th>Dasar</th>
          <th>Untuk</th>
          <th>Jabatan</th>
          <th>Golongan</th>
        </tr>
      </thead>
      <tbody>
        <?php $n=0;
          foreach($perintah->result_array() as $d){ 
        $n++;?>
        <tr>
          <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
          <td style="vertical-align: middle;"><?php echo $d['nama'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nip'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['nosurat'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['tglsurat'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['dasar'];?></td>
          <td style="vertical-align: middle;"><?php echo $d['untuk'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['jabatan'];?></td>
          <td style="text-align: center; vertical-align: middle;"><?php echo $d['golongan'];?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>