<!DOCTYPE html>
<html>
  <head>
    <title>Halaman Petugas SPPD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logopemkabtasik.png">

    <!-- third party css -->
    <link href="<?php echo base_url().'assets/custom-bootstrap/superadmin/css/vendor/jquery-jvectormap-1.2.2.css' ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/vendor/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
      
    <!-- Custom Style -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.css"></script>
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/petugas/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">

    <!-- App css -->
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/css/app.min.css" rel="stylesheet" type="text/css" />

  </head>
  <body>
      
        <!-- Begin page -->
        <div class="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu" style="background: #2d89ef">

                <div class="slimscroll-menu">

                    <!-- LOGO -->
                    <a href="#" class="logo text-center">
                        <span class="logo-lg">
                            <img src="<?php echo base_url(); ?>assets/img/logopemkabtasik.png" alt="" height="50">
                        </span>
                        <h4 style="color: white; margin-top: -5px;"> SPPD TASIKMALAYA </h4>
                        <span class="logo-sm">
                            <img src="assets/images/logo_sm.png" alt="" height="16">
                        </span>
                    </a>
                    
                    <hr>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav">

                        <li class="side-nav-title side-nav-item" style="color: #b8d7fa">Main Menu</li>

                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'petugas/inputsppdperintah'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account-box"></i>
                                <span> Buat SPPD Perintah </span>
                            </a>
                        </li>
                        
                        <li class="side-nav-item">
                            <a href="<?php echo base_url().'petugas/inputsppdtugas'; ?>" class="side-nav-link">
                                <i class="mdi mdi-account-box"></i>
                                <span> Buat SPPD Tugas </span>
                            </a>
                        </li>
            
                    </ul>
                    
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Topbar Start -->
                    <div class="navbar-custom">
                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                    aria-expanded="false">
                                    <span class="account-user-avatar"> 
                                        <img src="<?php echo base_url(); ?>assets/img/logopemkabtasik.png" alt="user-image" class="rounded-circle">
                                    </span>
                                    <span style="vertical-align: middle">
                                        <B>Petugas</B>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="mdi mdi-account-circle"></i>
                                        <span>My Account</span>
                                    </a>

                                    <!-- item-->
                                    <a href="<?php echo base_url().'login/logout'?>" class="dropdown-item notify-item">
                                        <i class="mdi mdi-logout"></i>
                                        <span>Logout</span>
                                    </a>

                                </div>
                            </li>

                        </ul>
                        <button class="button-menu-mobile open-left disable-btn">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </div>
                    <!-- end Topbar -->
                    
                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <a href="<?php echo site_url('petugas');?>" class="btn btn-danger pull-right" style="border-radius: 50px; border: none"><i class="fas fa-user-plus"></i> Kembali </a>
                                    </div>
                                    <h4 class="page-title">Buat SPPD Perintah Baru</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col">
                                <div class="card widget-flat">
                                    <div class="card-body">
                                        <div class="container register">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                        <h3 class="register-heading">SPPD Perintah</h3><br>
                                                            <?php echo $alert; ?>
                                                        <div class="row">
                                                            <div class="col">
                                                                <form class="form-horizontal" method="post">
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputNIP" class="col-md-2 col-form-label">NIP</label>
                                                                        <div class="col-3">
                                                                            <input type="number" name="nip" class="form-control" id="nip" placeholder="NIP" value="<?php echo $perintah['nip']; ?>" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputNama" class="col-md-2 col-form-label">Nama</label>
                                                                        <div class="col-3">
                                                                            <input type="text" name="nama" class="form-control" id="inputNama" placeholder="Nama" value="<?php echo $perintah['nama']; ?>" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputGolongan" class="col-md-2 col-form-label">Golongan</label>
                                                                        <div class="col-3">
                                                                            <select class="form-control" name="golongan" id="example-select" required>
                                                                                <option>Golongan I</option>
                                                                                <option>Golongan II</option>
                                                                                <option>Golongan III</option>
                                                                                <option>Golongan IV</option>
                                                                                <option>Golongan V</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputJabatan" class="col-md-2 col-form-label">Jabatan</label>
                                                                        <div class="col-3">
                                                                            <select class="form-control" name="jabatan" id="example-select" required>
                                                                                <option>Kepala Dinas</option>
                                                                                <option>Sekretaris Dinas</option>
                                                                                <option>Petugas Dinas</option>
                                                                                <option>Pegawai Dinas</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputDasar" class="col-md-2 col-form-label">Dasar</label>
                                                                        <div class="col-9">
                                                                            <input type="text" name="dasar" class="form-control" id="inputDasar" placeholder="Dasar" value="<?php echo $perintah['dasar']; ?>" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row mb-2">
                                                                        <label for="inputUntuk" class="col-md-2 col-form-label">Untuk</label>
                                                                        <div class="col-9">
                                                                            <input type="text" name="untuk" class="form-control" id="inputUntuk" placeholder="Untuk Melakukan" value="<?php echo $perintah['untuk']; ?>" required><br>
                                                                            <input type="hidden" name="id_sppdperintah" value="<?php echo $perintah['id_sppdperintah']; ?>">
                                                                            <input type="hidden" name="statusperintah" value="1">
                                                                            <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- container -->

                </div>
                <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2018 © Hyper - Coderthemes.com
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-md-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- App js -->
        <script src="<?php echo base_url(); ?>assets/custom-bootstrap/superadmin/js/app.min.js"></script>

        <!-- third party js -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/jquery.dataTables.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.bootstrap4.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.responsive.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/responsive.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.buttons.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.bootstrap4.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.html5.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.flash.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/buttons.print.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.keyTable.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/vendor/dataTables.select.min.js' ?>"></script>
        <script src="<?php echo base_url().'assets/js/jquery.js' ?>"></script>
        <script src="<?php echo base_url().'assets/custom-bootstrap/petugas/jquery-3.3.1.js' ?>"></script>
      
        <script src="<?php echo base_url().'assets/custom-bootstrap/petugas/jquery-ui-custom/jquery-ui.js' ?>"></script>
        <script src="<?php echo base_url().'assets/js/bootstrap.js' ?>"></script>
        <!-- third party js ends -->

        <!-- demo app -->
        <script src="<?php echo base_url().'assets/custom-bootstrap/superadmin/js/pages/demo.datatable-init.js' ?>"></script>
        <!-- end demo js-->
      
        <script type="text/javascript">
        $(document).ready(function(){
 
            $('#nip').autocomplete({
                source: "<?php echo site_url('petugas/get_autocomplete');?>",
      
                select: function (event, ui) {
                    $('[name="nip"]').val(ui.item.nip); 
                    $('[name="nama"]').val(ui.item.nama);
                    $('[name="jabatan"]').val(ui.item.jabatan); 
                    $('[name="golongan"]').val(ui.item.golongan); 
                }
            });
 
        });
    </script>

  </body>
</html>
