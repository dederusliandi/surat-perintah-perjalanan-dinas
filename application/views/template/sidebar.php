             <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!-- LOGO -->
                    <a href="index-2.html" class="logo text-center">
                        <span class="logo-lg">
                            <img src="<?= base_url('assets/vendor/hyper/'); ?>assets/images/logo.png" alt="" height="16">
                        </span>
                        <span class="logo-sm">
                            <img src="<?= base_url('assets/vendor/hyper/'); ?>assets/images/logo_sm.png" alt="" height="16">
                        </span>
                    </a>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav">

                        <li class="side-nav-title side-nav-item">Navigation</li>
                        <?php
                            if($this->session->userdata('id_role') == 1) {
                        ?>
                        <li class="side-nav-item">
                            <a href="<?= base_url('administrator'); ?>" class="side-nav-link">
                                <i class="dripicons-meter"></i>
                                <span> Dashboard </span>
                            </a>
                        </li>
                            <?php
                        } else if($this->session->userdata('id_role') == 2) {
                            ?>
                        
                        <li class="side-nav-item">
                            <a href="<?= base_url('kadis'); ?>" class="side-nav-link">
                                <i class="dripicons-meter"></i>
                                <span> Dashboard </span>
                            </a>
                        </li>

                        <li class="side-nav-item">
                            <a href="javascript: void(0);" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span> SPPD </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="#">Perintah</a>
                                </li>
                                <li>
                                    <a href="#">Tugas</a>
                                </li>
                            </ul>
                        </li>
                        
                        <?php
                        } else if($this->session->userdata('id_role') == 3) {
                            ?>
                        
                        
                        
                        <?php
                        } else if($this->session->userdata('id_role') == 4) {
                            ?>
                        
                            <li class="side-nav-item">
                                <a href="<?= base_url('bendahara'); ?>" class="side-nav-link">
                                    <i class="dripicons-meter"></i>
                                    <span> Dashboard </span>
                                </a>
                            </li>

                            <li class="side-nav-item">
                                <a href="javascript: void(0);" class="side-nav-link">
                                    <i class="dripicons-view-apps"></i>
                                    <span>Biaya SPPD</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="side-nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">Perintah</a>
                                    </li>
                                    <li>
                                        <a href="#">Tugas</a>
                                    </li>
                                </ul>
                            </li>
                        <?php
                        } else if($this->session->userdata('id_role') == 5) {
                            ?>
                        
                        <li class="side-nav-item">
                            <a href="<?= base_url('petugas'); ?>" class="side-nav-link">
                                <i class="dripicons-meter"></i>
                                <span> Dashboard </span>
                            </a>
                        </li>
                        
                        <?php
                        } else {
                            ?>
                        
                        
                        
                        <?php
                        }
                            ?>
                        
                        <!--<li class="side-nav-item">
                            <a href="javascript: void(0);" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span> Apps </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="apps-calendar.html">Calendar</a>
                                </li>
                                <li class="side-nav-item">
                                    <a href="javascript: void(0);" aria-expanded="false">Projects
                                        <span class="menu-arrow"></span>
                                    </a>
                                    <ul class="side-nav-third-level" aria-expanded="false">
                                        <li>
                                            <a href="apps-projects-list.html">List</a>
                                        </li>
                                        <li>
                                            <a href="apps-projects-details.html">Details</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="apps-tasks.html">Tasks</a>
                                </li>
                                <li class="side-nav-item">
                                    <a href="javascript: void(0);" aria-expanded="false">eCommerce
                                        <span class="menu-arrow"></span>
                                    </a>
                                    <ul class="side-nav-third-level" aria-expanded="false">
                                        <li>
                                            <a href="apps-ecommerce-products.html">Products</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-products-details.html">Products Details</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-orders.html">Orders</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-orders-details.html">Order Details</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-customers.html">Customers</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-shopping-cart.html">Shopping Cart</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-checkout.html">Checkout</a>
                                        </li>
                                        <li>
                                            <a href="apps-ecommerce-sellers.html">Sellers</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>-->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->