
                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2019 © SPPD Diskominfo Kabupaten Tasikmalaya
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-md-block">
                                    <a href="javascript: void(0);">About</a>
                                    <a href="javascript: void(0);">Support</a>
                                    <a href="javascript: void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- App js -->
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/app.min.js"></script>

        <!-- third party js -->
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/jquery.dataTables.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/dataTables.bootstrap4.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/dataTables.responsive.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/responsive.bootstrap4.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/dataTables.buttons.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/buttons.bootstrap4.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/buttons.html5.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/buttons.flash.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/buttons.print.min.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/vendor/dataTables.keyTable.min.js"></script>
        <!-- third party js ends -->

        <!-- demo app -->
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/pages/demo.datatable-init.js"></script>
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/pages/demo.toastr.js"></script>
        <!-- end demo js-->

        
        <script src="<?php echo base_url().'assets/vendor/hyper/assets/js/sweetalert2/sweetalert2.all.min.js' ?>"></script>
        
        <script src="<?php echo base_url().'assets/vendor/hyper/assets/js/myscript.js' ?>"></script>
        

    </body>

</html>
