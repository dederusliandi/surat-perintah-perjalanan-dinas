<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/hyper/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Nov 2018 05:51:20 GMT -->
<head>
        <meta charset="utf-8" />
        <title><?= $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url('assets/vendor/hyper/'); ?>assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/vendor/hyper/'); ?>assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Sign In</h4>
                                    <p class="text-muted mb-4">Aplikasi SPPD Online Diskominfo Kabupaten Tasikmalaya</p>
                                </div>

                                <form action="<?= base_url('login/do_login'); ?>" method="post">
                                    <?= $this->session->flashdata('msg'); ?>
                                    <div class="form-group">
                                        <label for="emailaddress">Username</label>
                                        <input class="form-control" type="text" name="username" id="username" placeholder="Username" value="<?= set_value('username'); ?>" autofocus required>
                                        <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" name="password" id="password" placeholder="Password" value="<?= set_value('password'); ?>" required>
                                        <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <!--<div class="form-group">
                                        <a href="#" class="text-muted float-right" style="margin-top: -15px"><small>Forgot your password?</small></a><br>
                                    </div>-->

                                    <div class="form-group text-center">
                                        <button class="btn btn-primary" type="submit"> Log In </button>
                                    </div>

                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <footer class="footer footer-alt" style="margin-top: 5px">
            2019 © SPPD Diskominfo Kabupaten Tasikmalaya
        </footer>

        <!-- App js -->
        <script src="<?= base_url('assets/vendor/hyper/'); ?>assets/js/app.min.js"></script>
    </body>

<!-- Mirrored from coderthemes.com/hyper/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Nov 2018 05:51:20 GMT -->
</html>
