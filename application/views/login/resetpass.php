<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login SPPD</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/logopemkabtasik.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/vendor/bootstrap/css/bootstrap.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/fonts/font-awesome-4.7.0/css/font-awesome.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/vendor/animate/animate.css'?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/vendor/css-hamburgers/hamburgers.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/vendor/select2/select2.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/css/util.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/bootstrap-new/css/main.css'?>">
<!--===============================================================================================-->
    
    <style type="text/css">
        img{
            -webkit-animation: mover 2s infinite  alternate;
            animation: mover 1s infinite  alternate;
        }
        
        @-webkit-keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
        
        @keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
    </style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">

				<form action="<?php echo base_url().'login/auth'?>" method="post" class="login100-form validate-form" style="margin-top: -100px; padding-bottom: 40px">
					<span class="login100-form-title">
						Forgot Password
					</span>
                    <span><center><?php echo $this->session->flashdata('msg');?></center></span><br>
					<div class="wrap-input100 validate-input" data-validate = "Email is required">
						<input class="input100" type="text" name="email" placeholder="Masukan email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn" style="margin-bottom: 5px">
						<button class="login100-form-btn" type="submit">
							Reset
						</button>
					</div>
                    <center><a href="<?= base_url('login') ?>">Back</a></center>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="<?php echo base_url().'assets/bootstrap-new/vendor/jquery/jquery-3.2.1.min.js'?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url().'assets/bootstrap-new/vendor/bootstrap/js/popper.js'?>"></script>
	<script src="<?php echo base_url().'vendor/bootstrap-new/bootstrap/js/bootstrap.min.js'?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url().'assets/bootstrap-new/vendor/select2/select2.min.js'?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url().'assets/bootstrap-new/vendor/tilt/tilt.jquery.min.js'?>"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url().'assets/bootstrap-new/js/main.js'?>"></script>

</body>
</html>