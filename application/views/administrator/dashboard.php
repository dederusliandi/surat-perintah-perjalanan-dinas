                    <!-- Start Content-->
                    <div class="container-fluid">
                                                
                        <?php if ($this->session->flashdata('flash')) : ?>
                        
                            <?= $this->session->flashdata('flash'); ?>
                        
                        <?php endif; ?>
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <a href="<?= site_url('administrator/createPegawai'); ?>" class="btn btn-sm btn-primary" style="margin-top: 25px">Tambah Akun</a>
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Data Pegawai</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <div class="table-responsive">
                                        <table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;">No</th>
                                                    <th>Nama</th>
                                                    <th>NIP</th>
                                                    <th>Jabatan</th>
                                                    <th>Golongan</th>
                                                    <th>Username</th>
                                                    <th>Password</th>
                                                    <th style="text-align: center;">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $n=0;
                                                foreach($utama as $d => $result){ 
                                            $n++;?>
                                                <tr>
                                                    <td style=" text-align: center; vertical-align: middle;"><?php echo $n;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->nama;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->nip;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->jabatan;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->golongan;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->username;?></td>
                                                    <td style=" vertical-align: middle;"><?php echo $result->password;?></td>
                                                    <td align="center">
                                                        <a href="<?php echo site_url('administrator/edit/'.$result->id_user);?>" class="action-icon" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="mdi mdi-square-edit-outline"></i></a>
                                                        <a href="<?php echo site_url('administrator/destroy/'.$result->id_user);?>" class="action-icon tombol-hapus" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>    
                                    </div>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->