<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Input Pegawai Dinas Baru</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col">
            <div class="card widget-flat">
                <div class="card-body">
                        <form class="form-horizontal" method="post" action="<?= site_url('administrator/update/'.$result->id); ?>">
                            <div class="form-group row mb-2">
                                <label for="inputNIM" class="col-md-2 col-form-label">NIP</label>
                                    <div class="col-10">
                                        <input type="number" name="nip" value="<?= $result->nip ?>" class="form-control" id="inputNIM" placeholder="NIP" required>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputNama" class="col-md-2 col-form-label">Nama</label>
                                    <div class="col-10">
                                        <input type="text" name="nama" value="<?= $result->nama ?>" class="form-control" id="inputNama" placeholder="Nama" required>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputLevel" class="col-md-2 col-form-label">Jabatan</label>
                                    <div class="col-10">
                                        <select class="form-control" name="jabatan" required>
                                            <option value="Kepala Dinas" <?= $result->jabatan == 'Kepala Dinas' ? 'selected' : '' ?>>Kepala Dinas</option>
                                            <option value="Sekretaris Dinas" <?= $result->jabatan == 'Sekretaris Dinas' ? 'selected' : '' ?>>Sekretaris Dinas</option>
                                            <option value="Bendahara Dinas" <?= $result->jabatan == 'Bendahara Dinas' ? 'selected' : '' ?>>Bendahara Dinas</option>
                                            <option value="Petugas Dinas" <?= $result->jabatan == 'Petugas Dinas' ? 'selected' : '' ?>>Petugas Dinas</option>
                                            <option value="Pegawai Dinas" <?= $result->jabatan == 'Pegawai Dinas' ? 'selected' : '' ?>>Pegawai Dinas</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputUsername" class="col-md-2 col-form-label">Golongan</label>
                                    <div class="col-10">
                                        <select class="form-control" name="golongan" required>
                                            <option value="Golongan I" <?= $result->golongan == 'Golongan I' ? 'selected' : '' ?>>Golongan I</option>
                                            <option value="Golongan II" <?= $result->golongan == 'Golongan II' ? 'selected' : '' ?>>Golongan II</option>
                                            <option value="Golongan III" <?= $result->golongan == 'Golongan III' ? 'selected' : '' ?>>Golongan III</option>
                                            <option value="Golongan IV" <?= $result->golongan == 'Golongan IV' ? 'selected' : '' ?>>Golongan IV</option>
                                            <option value="Golongan V" <?= $result->golongan == 'Golongan V' ? 'selected' : '' ?>>Golongan V</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputUsername" class="col-md-2 col-form-label">Username</label>
                                    <div class="col-10">
                                        <input type="text" name="username" value="<?= $result->username ?>" class="form-control" id="inputUsername" placeholder="Username" required>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputPassword5" class="col-md-2 col-form-label">Password</label>
                                    <div class="col-10">
                                        <input type="text" name="password" value="<?= $result->password ?>" class="form-control" id="inputPassword5" placeholder="Password" required>
                                    </div>
                            </div>
                            <div class="form-group row mb-2">
                                <label for="inputRole" class="col-md-2 col-form-label">Akses</label>
                                    <div class="col-10">
                                        <select class="form-control" name="id_role" required>
                                            <option value="2" <?= $result->id_role == 2 ? 'selected' : '' ?>>Kepala Dinas</option>
                                            <option value="3" <?= $result->id_role == 3 ? 'selected' : '' ?>>Sekretaris Dinas</option>
                                            <option value="4" <?= $result->id_role == 4 ? 'selected' : '' ?>>Bendahara Dinas</option>
                                            <option value="5" <?= $result->id_role == 5 ? 'selected' : '' ?>>Petugas Dinas</option>
                                            <option value="6" <?= $result->id_role == 6 ? 'selected' : '' ?>>Pegawai Dinas</option>
                                        </select>
                                        <br>
                                        <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                                        <a href="<?php echo site_url('administrator');?>" class="btn btn-danger">Kembali</a>
                                    </div>
                            </div>
                        </form>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>
        <!-- end col -->
    </div>
<!-- end row -->

</div>
<!-- container -->

</div>
<!-- content -->
