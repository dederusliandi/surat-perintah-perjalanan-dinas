                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Input Pegawai Baru Dinas</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col">
                                <div class="card widget-flat">
                                    <div class="card-body">
                                        <?php echo $alert;?>
                                        <form class="form-horizontal" method="post">
                                            <div class="form-group row mb-2">
                                                <label for="inputNIM" class="col-md-2 col-form-label">NIP</label>
                                                <div class="col-3">
                                                    <input type="number" name="nip" value="<?php echo $satu['nip'];?>" class="form-control" id="inputNIM" placeholder="NIP" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputNama" class="col-md-2 col-form-label">Nama</label>
                                                <div class="col-3">
                                                    <input type="text" name="nama" value="<?php echo $satu['nama'];?>" class="form-control" id="inputNama" placeholder="Nama" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputLevel" class="col-md-2 col-form-label">Jabatan</label>
                                                <div class="col-3">
                                                    <select class="form-control" name="jabatan" required>
                                                        <option value="Kepala Dinas">Kepala Dinas</option>
                                                        <option value="Sekretaris Dinas">Sekretaris Dinas</option>
                                                        <option value="Bendahara Dinas">Bendahara Dinas</option>
                                                        <option value="Petugas Dinas">Petugas Dinas</option>
                                                        <option value="Pegawai Dinas">Pegawai Dinas</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputUsername" class="col-md-2 col-form-label">Golongan</label>
                                                <div class="col-3">
                                                    <select class="form-control" name="golongan" required>
                                                        <option value="<?= $satu['golongan']; ?>">Golongan I</option>
                                                        <option value="<?= $satu['golongan']; ?>">Golongan II</option>
                                                        <option value="<?= $satu['golongan']; ?>">Golongan III</option>
                                                        <option value="<?= $satu['golongan']; ?>">Golongan IV</option>
                                                        <option value="<?= $satu['golongan']; ?>">Golongan V</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputUsername" class="col-md-2 col-form-label">Username</label>
                                                <div class="col-3">
                                                    <input type="text" name="username" value="<?php echo $satu['username'];?>" class="form-control" id="inputUsername" placeholder="Username" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputPassword5" class="col-md-2 col-form-label">Password</label>
                                                <div class="col-3">
                                                    <input type="password" name="password" value="<?php echo $satu['password'];?>" class="form-control" id="inputPassword5" placeholder="Password" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-2">
                                                <label for="inputRole" class="col-md-2 col-form-label">Akses</label>
                                                <div class="col-3">
                                                    <select class="form-control" name="id_role" required>
                                                        <option value="2">Kepala Dinas</option>
                                                        <option value="3">Sekretaris Dinas</option>
                                                        <option value="4">Bendahara Dinas</option>
                                                        <option value="5">Petugas Dinas</option>
                                                        <option value="6">Pegawai Dinas</option>
                                                    </select>
                                                    <br>
                                                    <input type="hidden" name="id_pegawai" value="<?php echo $satu['id_pegawai'];?>">
                                                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                                                    <a href="<?php echo site_url('administrator');?>" class="btn btn-danger">Kembali</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div>
                    <!-- container -->

                </div>
                <!-- content -->
