                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">SPPD</a></li>
                                            <li class="breadcrumb-item"><a href="#">SPPD Perintah</a></li>
                                            <li class="breadcrumb-item active">Tambah SPPD</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Tambah Rincian Biaya SPPD Tugas</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top: -10px">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <form class="form-horizontal" method="post" action="<?= site_url('bendahara/createbiayasppdtugas') ?>">
                                    <input type="hidden" name="id_surat" value="<?= $this->uri->segment(3) ?>">
                                        <div id="input-push">
                                            <div class="form-group row mb-2">
                                                <label for="inputNoSurat" class="col-md-2 col-form-label">Biaya</label>
                                                <div class="col-4">
                                                    <input type="text" name="rincian[]" class="form-control" value="" placeholder="Rincian" required>
                                                </div>
                                                <div class="col-4">
                                                    <input type="number" name="jumlah[]" class="form-control" value="" placeholder="Jumlah" required>
                                                </div>
                                                <div class="col-2">
                                                <button type="button" class="btn btn-primary" id="tambah">Tambah</button>
                                                </div> 
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
                <script>
                    
                    var i = 1;
                    $('#tambah').click(function() {
                        i++;
                        var html =  '<div class="form-group row mb-2" id="remove_'+i+'">';
                            html += '<label for="inputNoSurat" class="col-md-2 col-form-label">Biaya</label>';
                            html += '<div class="col-4">';
                            html += '<input type="text" name="rincian[]" class="form-control" value="" placeholder="Rincian" required>';
                            html += '</div>';
                            html += '<div class="col-4">';
                            html += '<input type="number" name="jumlah[]" class="form-control" value="" placeholder="Jumlah" required>';
                            html += '</div>';
                            html += '<div class="col-2">';
                            html += '<button type="button" class="remove_data btn btn-danger" id="'+i+'">hapus</button>';
                            html += '</div>';
                            html += '</div>';

                            $('#input-push').append(html);
                    });

                    $(document).on('click','.remove_data',function(){
                        var id = $(this).attr("id");
                        $('#remove_' + id).remove();
                        i -= 1;
                    });
                </script>