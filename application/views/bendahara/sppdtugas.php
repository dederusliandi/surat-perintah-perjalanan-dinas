                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="#">SPPD</a></li>
                                            <li class="breadcrumb-item"><a href="#">SPPD Tugas</a></li>
                                            <li class="breadcrumb-item active">Details SPPD</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Data SPPD Tugas</h4>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top: -10px">
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <table id="basic-datatable" class="table table-bordered table-sm dt-responsive nowrap">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>No. Surat</th>
                                                <th style="text-align: center;">Tanggal Surat</th>
                                                <th style="text-align: center;">Status Biaya</th>
                                                <th style="text-align: center;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $n=0;
                                          foreach($tugas as $d){ 
                                        $n++;?>
                                            <tr>
                                                <td style="text-align: center; vertical-align: middle;"><?php echo $n;?></td>
                                                <td style="vertical-align: middle;"><strong><?php echo $d->nosurat ?></strong></td>
                                                <td style="text-align: center; vertical-align: middle;">
                                                    <?php echo shortdate_indo($d->tglsurat);?>
                                                </td>
                                                    <?php
                                                        @$check = $this->db->where('id_surat', $d->id_surat)->get('biaya_sppd')->result()[0];
                                                    ?>
                                                <td>
                                                    <?php
                                                        if(!$check) {
                                                            echo "Belum ada rincian biaya";
                                                        } else {
                                                           if($check->status == 0) {
                                                            echo "<label class='label label-warning'>belum di acc</label>";
                                                           }  else {
                                                               echo "sudah di acc";
                                                           }
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                <?php
                                                       if(!$check) {
                                                           if($d->statussurat == 3) {
                                                            ?>
                                                                <a href="<?= site_url('bendahara/sppdpetugas/'.$d->id_surat.'/create-biaya') ?>" class="btn btn-primary">Buat Biaya</a>
                                                            <?php
                                                           } else {
                                                               echo "-";
                                                           }
                                                        } else {
                                                           if($check->status == 1) {
                                                            echo "<a target='_blank' href='".site_url('bendahara/pdfTugas/'.$check->id_biaya_sppd)."' class='btn btn-danger'>Print</a>";
                                                           } else {
                                                               echo "-";
                                                           }
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div> <!-- end card-->
                            </div> <!-- end col-->
                        </div>
                        <!-- end page title --> 
                        
                    </div> <!-- container -->

                </div> <!-- content -->