            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!-- LOGO -->
                    <a href="index-2.html" class="logo text-center">
                        <span class="logo-lg">
                            <img src="<?= base_url('assets/vendor/hyper/'); ?>assets/images/logo.png" alt="" height="16">
                        </span>
                        <span class="logo-sm">
                            <img src="<?= base_url('assets/vendor/hyper/'); ?>assets/images/logo_sm.png" alt="" height="16">
                        </span>
                    </a>

                    <!--- Sidemenu -->
                    <ul class="metismenu side-nav">

                        <li class="side-nav-title side-nav-item">Navigation</li>

                        <li class="side-nav-item">
                            <a href="<?= base_url('petugas'); ?>" class="side-nav-link">
                                <i class="dripicons-meter"></i>
                                <span> Dashboard </span>
                            </a>
                        </li>

                        <li class="side-nav-item">
                            <a href="#" class="side-nav-link">
                                <i class="dripicons-view-apps"></i>
                                <span>Biaya SPPD</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="side-nav-second-level" aria-expanded="false">
                                
                                <li class="side-nav-item">
                                    <a href="<?= site_url('bendahara/sppdpetugas')?>" aria-expanded="false">SPPD Tugas
                                    </a>
                                </li>
                                <li class="side-nav-item">
                                    <a href="<?= site_url('bendahara/sppdperintah')?>" aria-expanded="false">SPPD Perintah
                                    </a>
                                </li>
                            </ul>
                        </li>

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->