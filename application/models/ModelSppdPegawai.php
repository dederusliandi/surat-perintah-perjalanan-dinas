<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ModelSppdPegawai extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		//melakukan koneksi database
		$this->load->database();
	}
	
    public function all(){
		
		return $this->db->get('sppd_pegawai');
	}
    
    function create($data){
        $this->db->insert("sppd_pegawai", $data);
    }

    function insertId(){
        return $this->db->insert_id();
    }

    public function getWhere($where){
		$this->db->where($where);
		return $this->db->get('sppd_pegawai');
	}
    
    public function jabatan(){
        $query = $this->db->query("SELECT * FROM sppd_pegawai");
        return $query->result();
    }
    
    function autocomplete($nip){
        $this->db->like('nip', $nip , 'both');
        $this->db->order_by('nip', 'ASC');
        $this->db->limit(10);
        return $this->db->get('sppd_pegawai')->result();
    }
    
    function simpan_upload($nip,$nama,$image){
        $data = array(
                'nip' => $nip,
                'nama' => $nama,
                'gambar' => $image
            );  
        $result= $this->db->insert('sppd_pegawai',$data);
        return $result;
    }
	
	function update($id, $data){
        $this->db->where($id);
        $this->db->update("sppd_pegawai", $data);
    }
    
	public function delete($where){
		//menghapus data pada tabel buku sesuai kriteria
		return $this->db->delete('sppd_pegawai',$where);
	}
	
}
?>