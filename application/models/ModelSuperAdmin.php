<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ModelSuperAdmin extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function all(){
		return $this->db->get('pegawai');
	}
    
    function create($data){
        $this->db->insert("user", $data);
    }
    
    function readWithUser($where = "", $order = ""){
        if(!empty($where)) $this->db->where($where);
        if(!empty($order)) $this->db->order_by($order);

        $this->db->select("pegawai.id_pegawai, pegawai.id_user as id, pegawai.nip, pegawai.nama, pegawai.golongan, pegawai.jabatan, user.id_user as id, user.username, user.password, user.id_role");
        $this->db->join("user", "user.id_user = pegawai.id_user");
        $query = $this->db->get("pegawai");

        if($query AND $query->num_rows() != 0) {
            return $query->result();
        } else {
            return array();
        }
    }
    
    function readUser(){
        $this->db->select('*');
        $this->db->from('pegawai');
        $this->db->join('user' , 'user.id_user = pegawai.id_user');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function utama(){
        $this->db->select('*');
        $this->db->from('pegawai');
        $this->db->join('user' , 'user.id_user = pegawai.id_user');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function admin(){
        return $this->db->get('superadmin');
    }
    
	public function getWhere($where){
		$this->db->where($where);
		return $this->db->get('pegawai');
	}
    
	function update($id, $data){
        
        $this->db->where($id);
        $this->db->update("user", $data);
    }
    
    function delete($id){
        
        $this->db->where($id);
        $this->db->delete("user");
    }
	
}
?>