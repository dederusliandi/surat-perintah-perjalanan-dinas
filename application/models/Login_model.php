<?php
class Login_model extends CI_Model{
	//cek username dan password Super Admin
//	function auth_superadmin($username,$password){
//		$query=$this->db->query("SELECT * FROM superadmin WHERE username='$username' AND password='$password' LIMIT 1");
//		return $query;
//	}
//
//	//cek username dan password Pegawai
//	function auth_pegawai($username,$password){
//		$query=$this->db->query("SELECT * FROM pegawai WHERE username='$username' AND password='$password' LIMIT 1");
//		return $query;
//	}
//    
//    public function reset_pass($email, $reset_key){
//        $this->db->where('email', $email);
//        $data = array('reset_password' => $reset_key);
//        $this->db->update('pegawai', $data);
//        if ($this->db->affected_rows() > 0){
//            return TRUE;
//        } else {
//            return FALSE;
//        }
//    }
    
    function create($data){
        $this->db->insert("user", $data);
    }

    function insertId(){
        return $this->db->insert_id();
    }
    
    function update($id, $data){
        $this->db->where($id);
        $this->db->update("user", $data);
    }

    function delete($id){
        $this->db->where($id);
        $this->db->delete("user");
    }
    
    function read($where = "", $order = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);

            $query = $this->db->get("user");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }
    
}
