<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ModelSurat extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function daftarSppd() {
		$this->db->select('*');
		$this->db->from('sppd');
		$query = $this->db->get();
		return $query->result();
	}
    
    public function hitungJumlahSppdPerintah(){   
        $query = $this->db->query("SELECT * FROM sppd WHERE jenissurat = 1");
        return $query->num_rows();
    }
    
    public function hitungJumlahSppdTugas(){   
        $query = $this->db->query("SELECT * FROM sppd WHERE jenissurat = 2");
        return $query->num_rows();
    }
    
    public function sppdperintah($where = ""){
        $this->db->select('*');
        $this->db->from('sppd');
        $this->db->where('jenissurat = 1');
    
        if(!empty($where)) $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }
    
    public function sppdtugas($where = ""){
        $this->db->select('*');
        $this->db->from('sppd');
        $this->db->where('jenissurat = 2');

        if(!empty($where)) $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }
    
    function code_otomatis(){
        $this->db->select('Right(sppd.nosurat,3) as kode ',false);
        $this->db->order_by('id_surat', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('sppd');
        if($query->num_rows()<>0){
            $data = $query->row();
            $kode = intval($data->kode)+1;
        }else{
            $kode = 1;

        }
        
        $tgl = date('Y');
        $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
        $kodejadi  = "SPPD/".$tgl."/".$kodemax;
        return $kodejadi;

    }
    
    public function get_all(){
        // $this->db->select('sppd.*, pegawai.nama, pegawai.nip');
        // $this->db->from('sppd');
        // $this->db->join('pegawai' , 'pegawai.id_pegawai = sppd.id_pegawai');
        // $query = $this->db->get();
        // return $query->result();
    }
    
	public function all(){
		return $this->db->get('sppd');
	}
    
	public function getWhere($where){
		$this->db->where($where);
		return $this->db->get('sppd');
	}
    
	public function insertSppd($data){
        return $this->db->insert('sppd',$data);
	}
    
	public function update($data,$id){
        $this->db->where($id);
        $this->db->update("sppd", $data);
	}
	public function delete($where){
		return $this->db->delete('sppd',$where);
	}
	
}
?>