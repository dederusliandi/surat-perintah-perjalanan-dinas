<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BiayaSppd extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		//melakukan koneksi database
		$this->load->database();
	}
	
    public function all(){
		
		return $this->db->get('biaya_sppd');
	}
    
    function create($data){
        $this->db->insert("biaya_sppd", $data);
    }

    function insertId(){
        return $this->db->insert_id();
    }
    
    public function jabatan(){
        $query = $this->db->query("SELECT * FROM biaya_sppd");
        return $query->result();
    }
    
    function autocomplete($nip){
        $this->db->like('nip', $nip , 'both');
        $this->db->order_by('nip', 'ASC');
        $this->db->limit(10);
        return $this->db->get('biaya_sppd')->result();
    }
    
    function simpan_upload($nip,$nama,$image){
        $data = array(
                'nip' => $nip,
                'nama' => $nama,
                'gambar' => $image
            );  
        $result= $this->db->insert('biaya_sppd',$data);
        return $result;
    }
    
	public function getWhere($where){
		$this->db->where($where);
		return $this->db->get('biaya_sppd, user');
	}
	
	function update($id, $data){
        $this->db->where($id);
        $this->db->update("biaya_sppd", $data);
    }
    
	public function delete($where){
		//menghapus data pada tabel buku sesuai kriteria
		return $this->db->delete('biaya_sppd',$where);
	}
	
}
?>