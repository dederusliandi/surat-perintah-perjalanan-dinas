<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('DOMPDF_ENABLE_AUTOLOAD', false);
define("DOMPDF_ENABLE_REMOTE", false);
require_once("./vendor/dompdf/dompdf/dompdf_config.inc.php");

use Dompdf\Adapter\CPDF;
use Dompdf\Dompdf;
use Dompdf\Exception;

class Pdfgenerator {
    
    public function generate($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "landscape", $custom_size=array()) {
        $dompdf = new Dompdf();
        $dompdf->set_option('enable_html5_parser', true);
        $dompdf->load_html($html);
        if (!empty($custom_size)) {
            $dompdf->set_paper($custom_size, $orientation);
        } else {
            $dompdf->set_paper($paper, $orientation);
        }
        $dompdf->render();
        if ($stream) {
            $dompdf->stream($filename.".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }
    
}