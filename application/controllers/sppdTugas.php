<?php
class sppdTugas extends CI_Controller{

  private $alert = '';
  
  function __construct(){
    parent::__construct();
    //validasi jika user belum login
    if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}

    $this->load->library('upload');
    $this->load->model('ModelsppdTugas');
  }

  function index(){
    $data['tugas'] = $this->ModelsppdTugas->all();
    $this->load->view('v-sppdtugas',$data);
  }

  private function alert($open_tag=null,$close_tag=null,$data=null){ 
  //method ini untuk membuat alert yang dapat digunakan pada method lain
    if($data!=null) $data = $open_tag.$data.$close_tag;
    return $data;
    //contoh : $this->alert('<h1>','</h1>','Hello world'); Output : <h1>Hello World</h1>
  }

  public function eksporsppdTugas(){
    $data = array( 'title' => 'Laporan Excel SPPD Tugas',
    'sppdTugas' => $this->ModelsppdTugas->listingsppdTugas());
    $data['semua'] = $this->ModelsppdTugas->all();
    $this->load->view('laporan_excelsppdTugas',$data);
  }
    
  public function inputsppdTugas(){

    //jika form disubmit
    if($this->input->post('simpan')){
      //masukkan data POST ke dalam variabel $array
      $array = array(
          'nama'=>$this->input->post('nama'),
          'nip'=>$this->input->post('nip'),
          'nosurat'=>$this->input->post('nosurat'),
          'tglsurat'=>$this->input->post('tglsurat'),
          'keperluan'=>$this->input->post('keperluan'),
          'tujuan'=>$this->input->post('tujuan'),
          'lamahari'=>$this->input->post('lamahari'),
          'tglberangkat'=>$this->input->post('tglberangkat'),
          'tglkembali'=>$this->input->post('tglkembali'),
      );
      //jika id nya kosong ( melakukan insert data baru )
      if($this->input->post('id_sppdtugas')==''){
        //panggil method insert pada Model bukuModel
        if($this->ModelsppdTugas->insert($array)){
          //jika berhasil insert
          $this->alert = $this->alert("<p class='alert alert-success'>","</p>","Sukses Menyimpan"); 
        }else{
          //jika gagal insert
          $this->alert = $this->alert("<p class='alert alert-danger'>","</p>","Gagal Menyimpan");
        }
        // jika id nya tidak kosong ( melakukan update data )
      }else{
        //panggil method update pada Model bukuModel
        if($this->ModelsppdTugas->update($array,array('id_sppdtugas'=>$this->input->post('id_sppdtugas')))){
          //jika berhasil update
          $this->alert = $this->alert("<p class='alert alert-success'>","</p>","Sukses Menyimpan"); 
        }else{
          //jika gagal update
          $this->alert = $this->alert("<p class='alert alert-danger'>","</p>","Gagal Menyimpan");         
        }
      }
    }
    //memanggil method getWhere pada model bukuModel untuk memanggil data buku sesuai id
    //jika id kosong, maka nilai balik (return value) nya NULL
    //id diambil dari URL SEGMENT 3
    $data['satu'] = $this->ModelsppdTugas->getWhere(array('id_sppdtugas'=>$this->uri->segment(3)))->row_array();
    //masukkan data alert hasil proses
    $data['alert'] = $this->alert;
    //memanggil method template
    $this->load->view('form-inputsppdTugas',$data);
  }

  
  public function hapussppdTugas(){
    //jika telah diset URI SEGMENT 3 (id buku) maka hapus data sesuai id yang diset
    //dengan memanggil method model bukuModel
    if($this->uri->segment(3)) $this->ModelsppdTugas->delete(array('id_sppdtugas'=>$this->uri->segment(3)));
    //kemudian dialihkan ke controller Buku method index
    redirect('sppdTugas');
  }


}
