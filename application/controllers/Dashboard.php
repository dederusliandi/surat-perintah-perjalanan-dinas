<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata("username")) redirect("login");
	}

	function index()
	{
        $data['view'] = 'dashboard/index';
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
        $this->load->view('dashboard/index', $data);
        $this->load->view('template/footer');
	}
}
