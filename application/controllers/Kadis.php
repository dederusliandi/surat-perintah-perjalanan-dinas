<?php
class Kadis extends CI_Controller{

    private $alert = '';

    function __construct()
    {
        parent::__construct();
        $this->load->model('ModelSurat');
        $this->load->model('ModelPegawai');
        $this->load->model('BiayaSppd');
        $this->load->helper('tgl_indo');
    }

    function index()
    {
        $data['surat'] = $this->ModelSurat->all();
        $data['hitungperintah'] = $this->ModelSurat->hitungJumlahSppdPerintah();
        $data['hitungtugas'] = $this->ModelSurat->hitungJumlahSppdTugas();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['title'] = 'Halaman Kepala Dinas | Dashboard SPPD';
        $data['top'] = 'Dashboard SPPD';
        $this->load->view('kadis/template/header',$data);
        $this->load->view('kadis/template/sidebar');
        $this->load->view('kadis/template/topbar');
        $this->load->view('kadis/all-sppd',$data);
        $this->load->view('template/footer');
    }

    public function sppdperintah()
    {
        $data['sppd'] = $this->ModelSurat->sppdPerintah();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['title'] = 'Halaman Kepala Dinas | SPPD Perintah';
        $data['top'] = 'SPPD Perintah';
        $this->load->view('kadis/template/header',$data);
        $this->load->view('kadis/template/sidebar');
        $this->load->view('kadis/template/topbar');
        $this->load->view('kadis/sppdperintah',$data);
        $this->load->view('template/footer');
    }

    public function sppdtugas()
    {
        $data['sppd'] = $this->ModelSurat->sppdtugas();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['title'] = 'Halaman Kepala Dinas | SPPD Tugas';
        $data['top'] = 'SPPD Tugas';
        $this->load->view('kadis/template/header',$data);
        $this->load->view('kadis/template/sidebar');
        $this->load->view('kadis/template/topbar');
        $this->load->view('kadis/sppdtugas',$data);
        $this->load->view('template/footer');
    }

    public function detailSuratPerintah($id)
    {
        $data['sppd'] = $this->ModelSurat->sppdPerintah("id_surat = '$id'")[0];
        $data['title'] = 'Halaman Kepala Dinas | Detail SPPD Tugas';
        $data['top'] = 'Detail Sppd Perintah';
        $this->load->view('kadis/template/header',$data);
        $this->load->view('kadis/template/sidebar');
        $this->load->view('kadis/template/topbar');
        $this->load->view('kadis/detailsppdperintah',$data);
        $this->load->view('template/footer');
    }

    public function detailSuratTugas($id)
    {
        $data['sppd'] = $this->ModelSurat->sppdtugas("id_surat = '$id'")[0];
        $data['title'] = 'Halaman Kepala Dinas | Detail SPPD Tugas';
        $data['top'] = 'SPPD Tugas';
        $this->load->view('kadis/template/header',$data);
        $this->load->view('kadis/template/sidebar');
        $this->load->view('kadis/template/topbar');
        $this->load->view('kadis/detailsppdtugas',$data);
        $this->load->view('template/footer');
    }

    public function updateStatusPerintah($id)
    {
        $this->db->where("id_surat = '$id'");
        $this->db->update("sppd", array(
            'statussurat' => $this->input->post('statussurat')
        ));

        redirect('kadis/sppdperintah');
    }


    public function updateStatusTugas($id)
    {
        $this->db->where("id_surat = '$id'");
        $this->db->update("sppd", array(
            'statussurat' => $this->input->post('statussurat')
        ));

        redirect('kadis/sppdtugas');
    }

    public function accBiayaTugas($id)
    {
        $this->BiayaSppd->update("id_surat = $id", array(
            'status' => 1
        ));

        redirect("kadis/sppdtugas");
    }

    public function tolakBiayaTugas($id)
    {
        $this->BiayaSppd->delete("id_surat = $id", array(
            'status' => 1
        ));

        redirect("kadis/sppdtugas");
    }
    

    public function accBiayaPerintah($id)
    {
        $this->BiayaSppd->update("id_surat = $id", array(
            'status' => 1
        ));

        redirect("kadis/sppdperintah");
    }

    public function tolakBiayaPerintah($id)
    {
        $this->BiayaSppd->delete("id_surat = $id", array(
            'status' => 1
        ));

        redirect("kadis/sppdperintah");
    }
}
