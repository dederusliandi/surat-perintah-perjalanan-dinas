<?php
class Pegawai extends CI_Controller{
    
  private $alert = '';
  
  function __construct(){
    parent::__construct();
    $this->load->library('upload');
    $this->load->model('ModelSurat');
    $this->load->model('ModelsppdPerintah');
    $this->load->model('ModelsppdTugas');
    $this->load->model('ModelPegawai');
  }
    
    public function lihatsuratperintah(){
        $this->load->view('lihatSuratPerintah');
    }

  function index(){
    $data['perintah'] = $this->ModelsppdPerintah->all();
    $data['tugas'] = $this->ModelsppdTugas->all();
    $data['sppd'] = $this->ModelSurat->all();
    $data['pegawai'] = $this->ModelPegawai->all(); 
    //$this->load->view('pegawai/v-sppdpegawai',$data);
  }
    
    function do_upload(){
        $config['upload_path']="./assets/images";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
 
            $nip = $this->input->post('nip');
            $nip = $this->input->post('nama');
            $image = $data['upload_data']['file_name']; 
             
            $result= $this->ModelPegawai->simpan_upload($nip,$nama,$image);
            echo json_decode($result);
        }
 
     }

  private function alert($open_tag=null,$close_tag=null,$data=null){ 
  //method ini untuk membuat alert yang dapat digunakan pada method lain
    if($data!=null) $data = $open_tag.$data.$close_tag;
    return $data;
  }


}
