<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller{
	
	private $alert = '';
	
	public function __construct(){ 
		parent::__construct();
        if(!$this->session->userdata("username","nama")) redirect("login");
        if($this->session->userdata("id_role") != 1) redirect("login/error");
        
		$this->load->helper('url');
		$this->load->model('ModelSuperAdmin');
        $this->load->model('ModelPegawai');
        $this->load->model('ModelKadis');
	}
	
	public function index(){
        $data['utama'] = $this->ModelSuperAdmin->utama();
        $data['superadmin'] = $this->ModelSuperAdmin->admin();
		$data['semua'] = $this->ModelSuperAdmin->all();
        $data['title'] = 'Halaman Dashboard Super Admin';
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('administrator/dashboard',$data);
        $this->load->view('template/footer');
	}
    
    public function createPegawai(){
        $data['title'] = 'Halaman Tambah Pegawai | Super Admin';
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('administrator/pegawai/create',$data);
        $this->load->view('template/footer');
    }
    
    function edit($id){
        $data['title'] = 'Halaman Edit Pegawai | Super Admin';
        $data['result'] = $this->ModelSuperAdmin->readUser();
        $data['result'] = $this->ModelSuperAdmin->readWithUser("pegawai.id_user = '$id'")[0];
        $this->load->view('template/header', $data);
        $this->load->view('template/sidebar');
        $this->load->view('template/topbar');
		$this->load->view('administrator/pegawai/edit',$data);
        $this->load->view('template/footer');
    }
    
    function update($id){
        // create user
       $user = array(
            'nama' => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'id_role' =>  $this->input->post('id_role')
        );
        $this->ModelSuperAdmin->update("id_user = '$id'", $user);

        // create pegawai
        $pegawai = array(
            'nip' => $this->input->post('nip'),
            'nama' => $this->input->post('nama'),
            'jabatan' => $this->input->post('jabatan'),
            'golongan' => $this->input->post('golongan')
        );
        $this->ModelPegawai->update("id_pegawai = '$id'", $pegawai);

        $this->session->set_flashdata('flash', '<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Selamat - </strong> Data pegawai berhasil diubah!
                                        </div>');

        redirect('administrator');
    }
    
    function destroy($id){
        
        $this->ModelSuperAdmin->delete("id_user = '$id'");
        $this->session->set_flashdata('flash', '<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Selamat - </strong> Data pegawai berhasil dihapus!
                                        </div>');

        redirect('administrator');
    }
    
    public function store(){
        
        // create user
        $this->ModelSuperAdmin->create(array(
            'nama' => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'id_role' => $this->input->post('id_role')
        ));
        
        // create pegawai
        $this->ModelPegawai->create(array(
            'nip' => $this->input->post('nip'),
            'nama' => $this->input->post('nama'),
            'jabatan' => $this->input->post('jabatan'),
            'golongan' => $this->input->post('golongan'),
            'id_user' => $this->ModelPegawai->insertId()
        ));

        $this->session->set_flashdata('flash', '<div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Selamat - </strong> Data pegawai berhasil ditambah!
                                        </div>');


        redirect('administrator');
    }
    
}

?>