<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bendahara extends CI_Controller{
	
	public function __construct(){ 
		parent::__construct();
        if(!$this->session->userdata("username","nama")) redirect("login");
        if($this->session->userdata("id_role") != 4) redirect("login/error");
        $this->load->library('pdf');
         
        $this->load->model('ModelSurat');
        $this->load->model('ModelPegawai');
        $this->load->model('BiayaSppd');
        $this->load->model('DetailBiayaSppd');
        $this->load->helper('tgl_indo');
        
	}
	
    public function index()
    {
        $data['top'] = 'Bendahara';
        $data['title'] = 'Halaman Utama Bendahara';
        $this->load->view('bendahara/template/header', $data);
        $this->load->view('bendahara/template/sidebar');
        $this->load->view('bendahara/template/topbar');
		$this->load->view('bendahara/dashboard',$data);
        $this->load->view('bendahara/template/footer');
    }

    public function sppdpetugas()
    {
        $data['sppd'] = $this->ModelSurat->get_all();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['tugas'] = $this->ModelSurat->sppdtugas();
        $data['title'] = 'Halaman Bendahara | SPPD Tugas';
        $data['top'] = 'SPPD Tugas';
        $this->load->view('bendahara/template/header',$data);
        $this->load->view('bendahara/template/sidebar');
        $this->load->view('bendahara/template/topbar');
        $this->load->view('bendahara/sppdtugas',$data);
        $this->load->view('bendahara/template/footer');
    }

    public function sppdperintah()
    {
        $data['sppd'] = $this->ModelSurat->get_all();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['perintah'] = $this->ModelSurat->sppdperintah();
        $data['title'] = 'Halaman Bendahara | SPPD Perintah';
        $data['top'] = 'SPPD Perintah';
        $this->load->view('bendahara/template/header',$data);
        $this->load->view('bendahara/template/sidebar');
        $this->load->view('bendahara/template/topbar');
        $this->load->view('bendahara/sppdperintah',$data);
        $this->load->view('bendahara/template/footer');
    }

    public function createBiayaTugas()
    {
        $data['sppd'] = $this->ModelSurat->get_all();
        $data['title'] = 'Halaman Bendahara | SPPD Petugas - Create Rincian Biaya';
        $data['top'] = 'SPPD Perintah';
        $this->load->view('bendahara/template/header',$data);
        $this->load->view('bendahara/template/sidebar');
        $this->load->view('bendahara/template/topbar');
        $this->load->view('bendahara/inputbiayasppdtugas',$data);
        $this->load->view('bendahara/template/footer');
    }

    public function createBiayaPerintah()
    {
        $data['sppd'] = $this->ModelSurat->get_all();
        $data['title'] = 'Halaman Bendahara | SPPD Petugas - Create Rincian Biaya';
        $data['top'] = 'SPPD Perintah';
        $this->load->view('bendahara/template/header',$data);
        $this->load->view('bendahara/template/sidebar');
        $this->load->view('bendahara/template/topbar');
        $this->load->view('bendahara/inputbiayasppdperintah',$data);
        $this->load->view('bendahara/template/footer');
    }

    public function createbiayasppdperintah()
    {
        $sppd = $this->BiayaSppd->create(array(
            'id_surat' => $this->input->post('id_surat'),
            'tanggal_biaya' => date('Y-m-d'),
        ));

        $id = $this->db->insert_id();

        for($i = 0; $i < count($this->input->post('rincian')); $i++) {
            $this->DetailBiayaSppd->create(array(
                'id_biaya_sppd' => $id,
                'rincian' => $this->input->post('rincian')[$i],
                'jumlah' => $this->input->post('jumlah')[$i],
            ));
        }

        redirect('bendahara/sppdperintah');
    }

    public function createbiayasppdtugas()
    {
        $sppd = $this->BiayaSppd->create(array(
            'id_surat' => $this->input->post('id_surat'),
            'tanggal_biaya' => date('Y-m-d'),
        ));

        $id = $this->db->insert_id();

        for($i = 0; $i < count($this->input->post('rincian')); $i++) {
            $this->DetailBiayaSppd->create(array(
                'id_biaya_sppd' => $id,
                'rincian' => $this->input->post('rincian')[$i],
                'jumlah' => $this->input->post('jumlah')[$i],
            ));
        }

        redirect('bendahara/sppdpetugas');
    }

    public function pdfTugas($id)
	{
        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(275,7,'Surat Tugas Perjalanan Dinas',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(275,7,'Rincian Biaya',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        @$biayasppd = $this->db->where('id_biaya_sppd', $id)->get('biaya_sppd')->result()[0];
        @$sppd = $this->db->where('id_surat', @$biayasppd->id_surat)->get('sppd')->result()[0];

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6,'No SPPD');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6,$sppd->nosurat);
        $pdf->Ln(6);

        $pdf->Cell(25,6,'Tanggal');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6,date('d F Y', strtotime(@$biayasppd->tanggal_biaya)));
        $pdf->Ln(6);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6, 'Bendahara');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6, $this->session->userdata("nama"));
        $pdf->Ln(10);
        
        $pdf->Cell(20,6,'No',1,0,'C');
        $pdf->Cell(130,6,'Rincian',1,0,'C');
        $pdf->Cell(130,6,'Jumlah',1,1,'C');


        $dataBiayaSppd = $this->db->where('id_biaya_sppd', $id)->get('detail_biaya_sppd')->result();
        foreach ($dataBiayaSppd as $row => $result){
            $pdf->Cell(20,6,$row + 1,1,0,'C');
            $pdf->Cell(130,6,$result->rincian,1,0,'C');
            $pdf->Cell(130,6,'Rp. '.number_format($result->jumlah,0,',','.'),1,1,'C');
        }
        // $pdf->Ln(10);
       

        $pdf->Output('rincian-biaya-surat-tugas.pdf', 'D');
        // $pdf->Output();
    }
    
    public function pdfPerintah($id)
	{
        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(275,7,'Surat Perintah Perjalanan Dinas',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(275,7,'Rincian Biaya',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        @$biayasppd = $this->db->where('id_biaya_sppd', $id)->get('biaya_sppd')->result()[0];
        @$sppd = $this->db->where('id_surat', @$biayasppd->id_surat)->get('sppd')->result()[0];

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6,'No SPPD');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6,$sppd->nosurat);
        $pdf->Ln(6);

        $pdf->Cell(25,6,'Tanggal');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6,date('d F Y', strtotime(@$biayasppd->tanggal_biaya)));
        $pdf->Ln(6);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6, 'Bendahara');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6, $this->session->userdata("nama"));
        $pdf->Ln(10);
        
        $pdf->Cell(20,6,'No',1,0,'C');
        $pdf->Cell(130,6,'Rincian',1,0,'C');
        $pdf->Cell(130,6,'Jumlah',1,1,'C');


        $dataBiayaSppd = $this->db->where('id_biaya_sppd', $id)->get('detail_biaya_sppd')->result();
        foreach ($dataBiayaSppd as $row => $result){
            $pdf->Cell(20,6,$row + 1,1,0,'C');
            $pdf->Cell(130,6,$result->rincian,1,0,'C');
            $pdf->Cell(130,6,'Rp. '.number_format($result->jumlah,0,',','.'),1,1,'C');
        }
        // $pdf->Ln(10);
       

        $pdf->Output('rincian-biaya-surat-perintah.pdf', 'D');
        // $pdf->Output();
	}
    
}

?>