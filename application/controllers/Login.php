<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}

	function index(){
        $data['title'] = 'Halaman Login';
        $this->load->view('login/login', $data);
	}
    
    function do_login()
    {
        //ambil data dari v_login
		$username		= $this->input->post("username");
		$password		= $this->input->post("password");

		//ambil data berdasarkan where
		$where	= array(
			"username"		=> $username,
			"password"		=> $password
        );
        
		$result	= $this->login_model->read($where);

		if(count($result) != 0) {
			$this->session->set_userdata("username",$username);
			$id_user	= $result[0]->id;
			$nama	= $result[0]->nama;
            $this->session->set_userdata("nama",$nama);
			$this->session->set_userdata("id_user",$id_user);
			$id_role	= $result[0]->id_role;
			$this->session->set_userdata("id_role",$id_role);
			if($id_role == 1){
                redirect("administrator");
            } elseif ($id_role == 2){
                redirect("kadis");
            } elseif ($id_role == 4){
                redirect("bendahara");
            }elseif ($id_role == 5){
                redirect("petugas");
            } else{
                $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
                                                        Username atau password Tidak ada!
                                                    </div>');
                redirect("login");
            }
            
            //redirect("dashboard");

		} else {
			//feedback jika gagal
			$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
                                                        Username atau password salah!
                                                    </div>');
			//$this->session->set_flashdata("required","Username atau Password harus di isi");
			redirect('login');
		}
    }
    
    function logout()
	{
		$this->session->sess_destroy();
		redirect("login");
	}
    
    public function error(){
        $data['title'] = 'Error 404';
        $this->load->view('login/error404', $data);
    }

//	function auth(){
//        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
//        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
// 
//        $cek_superadmin=$this->login_model->auth_superadmin($username,$password);
// 
//        if($cek_superadmin->num_rows() > 0){ //jika login sebagai superadmin
//                $data=$cek_superadmin->row_array();
//                $this->session->set_userdata('masuk',TRUE);
//                 if($data['level']=='1'){ //Akses superadmin
//                    $this->session->set_userdata('akses','1');
//                    $this->session->set_userdata('ses_id',$data['username']);
//                    $this->session->set_userdata('ses_nama',$data['nama']);
//                    redirect('administrator');
// 
//                 }else{ //akses superadmin
//                    $this->session->set_userdata('akses','1');
//                    $this->session->set_userdata('ses_id',$data['username']);
//                    $this->session->set_userdata('ses_nama',$data['nama']);
//                    redirect('administrator');
//                 }
// 
//        }else{ //jika login sebagai pegawai
//                    $cek_pegawai=$this->login_model->auth_pegawai($username,$password);
//					$data = $cek_pegawai->row_array();
//					$this->session->set_userdata('masuk',TRUE);
//					if($cek_pegawai->num_rows() > 0){
//							$data=$cek_pegawai->row_array();
//        					$this->session->set_userdata('masuk',TRUE);
//							$this->session->set_userdata('akses','0');
//							$this->session->set_userdata('ses_id',$data['username']);
//							$this->session->set_userdata('ses_nama',$data['nama']);
//
//					} if($data['jabatan'] == 'Kepala Dinas'){
//        					$this->session->set_userdata('masuk',TRUE);
//							$this->session->set_userdata('akses','2');
//							$this->session->set_userdata('ses_id',$data['username']);
//							$this->session->set_userdata('ses_nama',$data['nama']);
//							redirect('kadis');
//
//					} if($data['jabatan'] == 'Petugas Dinas'){
//        					$this->session->set_userdata('masuk',TRUE);
//							$this->session->set_userdata('akses','3');
//							$this->session->set_userdata('ses_id',$data['username']);
//							$this->session->set_userdata('ses_nama',$data['nama']);
//							redirect('petugas');
//
//					} if($data['jabatan'] == 'Sekretaris Dinas'){
//        					$this->session->set_userdata('masuk',TRUE);
//							$this->session->set_userdata('akses','4');
//							$this->session->set_userdata('ses_id',$data['username']);
//							$this->session->set_userdata('ses_nama',$data['nama']);
//							redirect('sekretaris');
//
//					} if($data['jabatan'] == 'Pegawai Dinas'){
//        					$this->session->set_userdata('masuk',TRUE);
//							$this->session->set_userdata('akses','5');
//							$this->session->set_userdata('ses_id',$data['username']);
//							$this->session->set_userdata('ses_nama',$data['nama']);
//							redirect('pegawai');
//
//					}
//            
//                    else{  // jika username dan password tidak ditemukan atau salah
//                        $url=base_url();
//                        echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Username atau password salah!</div>');
//                        redirect($url);
//                    }
//        }
// 
//    }

//    function logout(){
//        $this->session->unset_userdata('username');
//        $this->session->unset_userdata('level');
//        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda berhasil logout!</div>');
//        $url=base_url('');
//        redirect($url);
//    }
//    
//    public function forgotpass(){
//        $this->load->view('login/resetpass');
//    }
//    
//    	public function email_reset_password_validation(){
//		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
//		if($this->form_validation->run()){
//
//			$email = $this->input->post('email');
//			$reset_key =  random_string('alnum', 50);
//
//			if($this->Login_Model->reset_pass($email,$reset_key))
//			{
//				
//		    	$this->load->library('email');
//				$config = array();
//				$config['charset'] = 'utf-8';
//				$config['useragent'] = 'Codeigniter';
//				$config['protocol']= "smtp";
//				$config['mailtype']= "html";
//				$config['smtp_host']= "ssl://smtp.gmail.com";//pengaturan smtp
//				$config['smtp_port']= "465";
//				$config['smtp_timeout']= "5";
//				$config['smtp_user']= "iniuntukpercobaanaja@gmail.com"; // isi dengan email kamu
//				$config['smtp_pass']= "1sampai9"; // isi dengan password kamu
//				$config['crlf']="\r\n"; 
//				$config['newline']="\r\n"; 
//				$config['wordwrap'] = TRUE;
//				//memanggil library email dan set konfigurasi untuk pengiriman email
//					
//				$this->email->initialize($config);
//				//konfigurasi pengiriman
//				$this->email->from($config['smtp_user']);
//				$this->email->to($this->input->post('email'));
//				$this->email->subject("Reset your password");
//
//				$message = "<p>Anda melakukan permintaan reset password</p>";
//				$message .= "<a href='".site_url('welcome/reset_password/'.$reset_key)."'>klik reset password</a>";
//				$this->email->message($message);
//				
//				if($this->email->send())
//				{
//					echo "silahkan cek email <b>".$this->input->post('email').'</b> untuk melakukan reset password';
//				}else
//				{
//					echo "Berhasil melakukan registrasi, gagal mengirim verifikasi email";
//				}
//				
//				echo "<br><br><a href='".site_url("member-login")."'>Kembali ke Menu Login</a>";
//
//			}else {
//				die("Email yang anda masukan belum terdaftar");
//			}
//		} else{
//			$this->load->view('v_login');
//		}
//	}

}
