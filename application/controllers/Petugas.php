<?php
class Petugas extends CI_Controller{
    
    private $alert = '';
    
    function __construct(){
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        if($this->session->userdata("id_role") != 5) redirect("login/error");
        
        $this->load->model('ModelSurat');
        $this->load->model('ModelPegawai');
        $this->load->model('ModelSppdPegawai');
        $this->load->helper('tgl_indo');
        $this->load->library('pdf');
    }
    
    
    public function eksporsppdPerintah(){
        $data = array( 'title' => 'Laporan Excel SPPD Perintah',
        'petugas' => $this->ModelsppdPerintah->listingsppdPerintah());
        $data['perintah'] = $this->ModelsppdPerintah->all();
        $this->load->view('petugas/laporan_excelsppdPerintah',$data);
    }
    
    public function eksporsppdTugas(){
        $data = array( 'title' => 'Laporan Excel SPPD Tugas',
        'petugas' => $this->ModelsppdTugas->listingsppdTugas());
        $data['tugas'] = $this->ModelsppdTugas->all();
        $this->load->view('petugas/laporan_excelsppdTugas',$data);
    }

      function index(){
        $data['sppd'] = $this->ModelSurat->all();
        $data['pegawai'] = $this->ModelPegawai->all();
        $data['hitungperintah'] = $this->ModelSurat->hitungJumlahSppdPerintah();
        $data['hitungtugas'] = $this->ModelSurat->hitungJumlahSppdTugas();
        $data['title'] = 'Halaman Dashboard Petugas';
        $data['top'] = 'Dashboard SPPD';
        $this->load->view('petugas/template/header',$data);
        $this->load->view('petugas/template/sidebar');
        $this->load->view('petugas/template/topbar');
        $this->load->view('petugas/dashboard',$data);
        $this->load->view('petugas/template/footer');
      }
    
    public function sppdperintah(){
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['perintah'] = $this->ModelSurat->sppdperintah();
        $data['title'] = 'Halaman Petugas | SPPD Perintah';
        $data['top'] = 'SPPD Perintah';
        $this->load->view('petugas/template/header',$data);
        $this->load->view('petugas/template/sidebar');
        $this->load->view('petugas/template/topbar');
        $this->load->view('petugas/sppdperintah',$data);
        $this->load->view('petugas/template/footer');
    }
    
    public function sppdtugas(){
        $data['sppd'] = $this->ModelSurat->get_all();
        $data['jabatan'] = $this->ModelPegawai->jabatan();
        $data['tugas'] = $this->ModelSurat->sppdtugas();
        $data['title'] = 'Halaman Petugas | SPPD Tugas';
        $data['top'] = 'SPPD Tugas';
        $this->load->view('petugas/template/header',$data);
        $this->load->view('petugas/template/sidebar');
        $this->load->view('petugas/template/topbar');
        $this->load->view('petugas/sppdtugas',$data);
        $this->load->view('petugas/template/footer');
    }
    
//    function get_autocomplete(){
//        if (isset($_GET['term'])) {
//            $result = $this->blog_model->search_blog($_GET['term']);
//            if (count($result) > 0) {
//            foreach ($result as $row)
//                $arr_result[] = $row->blog_title;
//                echo json_encode($arr_result);
//            }
//        }
//    }
    
    public function printsuratperintah($id){

        $sppd = $this->ModelSurat->sppdperintah(array("id_surat" =>$id))[0];

         $dataPegawai = $this->db->select('pegawai.*')->where("sppd_pegawai.id_sppd", $id)->join("pegawai", "pegawai.id_pegawai = sppd_pegawai.id_pegawai")->get("sppd_pegawai")->result();
        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(180,7,'Surat Perintah',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(180,7,'Nomor : '.$sppd->nosurat,0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(25,6,'Dasar');
        $pdf->Cell(5,6,':');
        $pdf->Cell(170,6,$sppd->dasar);
        $pdf->Ln(10);

        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(180,7,'Memerintahkan',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','',12);
        foreach($dataPegawai as $row => $pegawai) {

          if($row == 0) {
            $pdf->Cell(30,6, 'Kepada','','','R');
          } else {
            $pdf->Cell(30,6, '','','','R');

          }
          $pdf->Cell(20,6, $row + 1,'','','R');
          $pdf->Cell(30,6, 'Nama','','','L');
          $pdf->Cell(20,6,':','','','C');
          $pdf->Cell(105,6, $pegawai->nama,'','','L');
          $pdf->Ln(6);

          $pdf->Cell(30,6, '','','','R');
          $pdf->Cell(20,6, '','','','R');
          $pdf->Cell(30,6, 'Pangkat/Gol','','','L');
          $pdf->Cell(20,6,':','','','C');
          $pdf->Cell(105,6, $pegawai->golongan,'','','L');
          $pdf->Ln(6);

          $pdf->Cell(30,6, '','','','R');
          $pdf->Cell(20,6, '','','','R');
          $pdf->Cell(30,6, 'NIP','','','L');
          $pdf->Cell(20,6,':','','','C');
          $pdf->Cell(105,6, $pegawai->nip,'','','L');
          $pdf->Ln(6);

          $pdf->Cell(30,6, '','','','R');
          $pdf->Cell(20,6, '','','','R');
          $pdf->Cell(30,6, 'Jabatan','','','L');
          $pdf->Cell(20,6,':','','','C');
          $pdf->Cell(105,6, $pegawai->jabatan,'','','L');
          $pdf->Ln(6);

          $pdf->Cell(30,6, '','','','R');
          $pdf->Cell(20,6, '','','','R');
          $pdf->Cell(30,6, '','','','L');
          $pdf->Cell(20,6,':','','','C');
          $pdf->Cell(105,6, '','','','L');
          $pdf->Ln(6);
        }

        foreach(explode(',', $sppd->untuk) as $row => $data) {
          $pdf->SetFont('Arial','',12);
          if($row == 0) {
            $pdf->Cell(25,6,'Untuk');
            $pdf->Cell(5,6,':');
          } else {
            $pdf->Cell(25,6,'');
            $pdf->Cell(5,6,'');
          }
         
          $pdf->Cell(5,6,($row+1).'.');
          $pdf->Cell(170,6,$data);
          $pdf->Ln(6);
        }

        $pdf->Output('surat-perintah.pdf', 'D');
        // $pdf->Output();
    }

    public function printsurattugas($id){

      $sppd = $this->ModelSurat->sppdtugas(array("id_surat" =>$id))[0];

       $dataPegawai = $this->db->select('pegawai.*')->where("sppd_pegawai.id_sppd", $id)->join("pegawai", "pegawai.id_pegawai = sppd_pegawai.id_pegawai")->get("sppd_pegawai")->result();
      $pdf = new FPDF('P','mm','A4');
      // membuat halaman baru
      $pdf->AddPage();
      // setting jenis font yang akan digunakan
      $pdf->SetFont('Arial','B',16);
      // mencetak string 
      $pdf->Cell(180,7,'Surat Tugas',0,1,'C');
      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(180,7,'Nomor : '.$sppd->nosurat,0,1,'C');
      // Memberikan space kebawah agar tidak terlalu rapat
      $pdf->Cell(10,7,'',0,1);

      $pdf->SetFont('Arial','',12);
      $pdf->Cell(25,6,'Dasar');
      $pdf->Cell(5,6,':');
      $pdf->Cell(170,6,$sppd->dasar);
      $pdf->Ln(10);

      $pdf->SetFont('Arial','B',12);
      $pdf->Cell(180,7,'Menugaskan',0,1,'C');
      // Memberikan space kebawah agar tidak terlalu rapat
      $pdf->Cell(10,7,'',0,1);

      $pdf->SetFont('Arial','',12);
      foreach($dataPegawai as $row => $pegawai) {

        if($row == 0) {
          $pdf->Cell(30,6, 'Kepada','','','R');
        } else {
          $pdf->Cell(30,6, '','','','R');

        }
        $pdf->Cell(20,6, $row + 1,'','','R');
        $pdf->Cell(30,6, 'Nama','','','L');
        $pdf->Cell(20,6,':','','','C');
        $pdf->Cell(105,6, $pegawai->nama,'','','L');
        $pdf->Ln(6);

        $pdf->Cell(30,6, '','','','R');
        $pdf->Cell(20,6, '','','','R');
        $pdf->Cell(30,6, 'Pangkat/Gol','','','L');
        $pdf->Cell(20,6,':','','','C');
        $pdf->Cell(105,6, $pegawai->golongan,'','','L');
        $pdf->Ln(6);

        $pdf->Cell(30,6, '','','','R');
        $pdf->Cell(20,6, '','','','R');
        $pdf->Cell(30,6, 'NIP','','','L');
        $pdf->Cell(20,6,':','','','C');
        $pdf->Cell(105,6, $pegawai->nip,'','','L');
        $pdf->Ln(6);

        $pdf->Cell(30,6, '','','','R');
        $pdf->Cell(20,6, '','','','R');
        $pdf->Cell(30,6, 'Jabatan','','','L');
        $pdf->Cell(20,6,':','','','C');
        $pdf->Cell(105,6, $pegawai->jabatan,'','','L');
        $pdf->Ln(6);

        $pdf->Cell(30,6, '','','','R');
        $pdf->Cell(20,6, '','','','R');
        $pdf->Cell(30,6, '','','','L');
        $pdf->Cell(20,6,':','','','C');
        $pdf->Cell(105,6, '','','','L');
        $pdf->Ln(6);
      }

      foreach(explode(',', $sppd->untuk) as $row => $data) {
        $pdf->SetFont('Arial','',12);
        if($row == 0) {
          $pdf->Cell(25,6,'Untuk');
          $pdf->Cell(5,6,':');
        } else {
          $pdf->Cell(25,6,'');
          $pdf->Cell(5,6,'');
        }
       
        $pdf->Cell(5,6,($row+1).'.');
        $pdf->Cell(170,6,$data);
        $pdf->Ln(6);
      }

      $pdf->Output('surat-tugas.pdf', 'D');
  }
    
    
    function printsurat(){
        $data['sppd'] = $this->ModelSurat->all();
        $this->load->view('lihatSurat', $data);
    }

  private function alert($open_tag=null,$close_tag=null,$data=null){ 
  //method ini untuk membuat alert yang dapat digunakan pada method lain
    if($data!=null) $data = $open_tag.$data.$close_tag;
    return $data;
  }
    
    public function inputsppdperintah($id = ''){

    //jika form disubmit
      if($this->input->post('simpan')){
          //masukkan data POST ke dalam variabel $array
          $tglsurat = date('Y-m-d');
          $sppd = array(
              'nosurat'=>$this->input->post('nosurat'),
              // 'id_pegawai'=>$this->input->post('id_pegawai'),
              'dasar'=>$this->input->post('dasar'),
              'untuk'=>$this->input->post('untuk'),
              'tglsurat' => $tglsurat,
              'statussurat'=>$this->input->post('statussurat'),
              'jenissurat'=>$this->input->post('jenissurat'),
          );
        
          //jika id nya kosong ( melakukan insert data baru )
          if($this->input->post('id_surat')==''){
            $data = $this->ModelSurat->insertSppd($sppd);
            
            $id = $this->db->insert_id();

            for($i = 0; $i < count($this->input->post('id_pegawai')); $i++) {
              $this->ModelSppdPegawai->create(array(
                  'id_sppd' => $id,
                  'id_pegawai' => $this->input->post('id_pegawai')[$i],
              ));
            }

            redirect('petugas/sppdperintah');
          }else{
            //panggil method update pada Model SPPD

             $this->ModelSurat->update($sppd,array('id_surat'=>$this->input->post('id_surat')));
      
              $this->ModelSppdPegawai->delete(array("id_sppd" => $id));
              for($i = 0; $i < count($this->input->post('id_pegawai')); $i++) {
                $this->ModelSppdPegawai->create(array(
                    'id_sppd' => $id,
                    'id_pegawai' => $this->input->post('id_pegawai')[$i],
                ));
              }

              //jika berhasil update
              $this->alert = $this->alert("<p class='alert alert-success'>","</p>","Data Sukses Diubah");
              redirect('petugas/sppdperintah');
          }
        }
        //memanggil method getWhere pada model SPPD untuk memanggil data buku sesuai id
        //jika id kosong, maka nilai balik (return value) nya NULL
        //id diambil dari URL SEGMENT 3
        $data['perintah'] = $this->ModelSurat->getWhere(array('id_surat'=>$this->uri->segment(3)))->row_array();
        //masukkan data alert hasil proses
        $data['alert'] = $this->alert;
        //memanggil method template
        $data['title'] = 'Halaman Petugas | SPPD Perintah';
        $data['top'] = 'SPPD Perintah';
        $data['pegawai'] = $this->ModelPegawai->all();
        $data['data_pegawai'] = $this->ModelSppdPegawai->getWhere(array('id_sppd' => $id))->result();
        $data['kodeunik'] = $this->ModelSurat->code_otomatis();
        $this->load->view('petugas/template/header', $data);
        $this->load->view('petugas/template/sidebar');
        $this->load->view('petugas/template/topbar', $data);
        $this->load->view('petugas/inputsppdperintah',$data);
        $this->load->view('petugas/template/footer');
    }
    
    public function detailsppdperintah(){
        $data['title'] = 'Halaman Petugas | SPPD Perintah';
        $data['top'] = 'Detail SPPD Perintah';
        $data['kodeunik'] = $this->ModelSurat->code_otomatis();
        $this->load->view('petugas/template/header', $data);
        $this->load->view('petugas/template/sidebar');
        $this->load->view('petugas/template/topbar', $data);
        $this->load->view('petugas/detailsppdperintah',$data);
        $this->load->view('petugas/template/footer');
    }
    
    public function inputsppdtugas($id = ''){

      //jika form disubmit
      if($this->input->post('simpan')){
        //masukkan data POST ke dalam variabel $array
        $tglsurat = date('Y-m-d');
        $sppd = array(
            'nosurat'=>$this->input->post('nosurat'),
            // 'id_pegawai'=>$this->input->post('id_pegawai'),
            'dasar'=>$this->input->post('dasar'),
            'untuk'=>$this->input->post('untuk'),
            'tglsurat' => $tglsurat,
            'statussurat'=>$this->input->post('statussurat'),
            'jenissurat'=>$this->input->post('jenissurat'),
        );
      
        //jika id nya kosong ( melakukan insert data baru )
        if($this->input->post('id_surat')==''){
          $data = $this->ModelSurat->insertSppd($sppd);
          
          $id = $this->db->insert_id();

          for($i = 0; $i < count($this->input->post('id_pegawai')); $i++) {
            $this->ModelSppdPegawai->create(array(
                'id_sppd' => $id,
                'id_pegawai' => $this->input->post('id_pegawai')[$i],
            ));
          }

          redirect('petugas/sppdtugas');
        }else{
          //panggil method update pada Model SPPD

          $this->ModelSurat->update($sppd,array('id_surat'=>$this->input->post('id_surat')));

            $this->ModelSppdPegawai->delete(array("id_sppd" => $id));
            for($i = 0; $i < count($this->input->post('id_pegawai')); $i++) {
              $this->ModelSppdPegawai->create(array(
                  'id_sppd' => $id,
                  'id_pegawai' => $this->input->post('id_pegawai')[$i],
              ));
            }

            //jika berhasil update
            $this->alert = $this->alert("<p class='alert alert-success'>","</p>","Data Sukses Diubah");
            redirect('petugas/sppdtugas');
        }
      }
      //memanggil method getWhere pada model SPPD untuk memanggil data buku sesuai id
      //jika id kosong, maka nilai balik (return value) nya NULL
      //id diambil dari URL SEGMENT 3
      $data['tugas'] = $this->ModelSurat->getWhere(array('id_surat'=>$this->uri->segment(3)))->row_array();
      //masukkan data alert hasil proses
      $data['alert'] = $this->alert;
      //memanggil method template
      $data['title'] = 'Halaman Petugas | SPPD tugas';
      $data['top'] = 'SPPD Perintah';
      $data['pegawai'] = $this->ModelPegawai->all();
      $data['data_pegawai'] = $this->ModelSppdPegawai->getWhere(array('id_sppd' => $id))->result();
      $data['kodeunik'] = $this->ModelSurat->code_otomatis();
      $this->load->view('petugas/template/header', $data);
      $this->load->view('petugas/template/sidebar');
      $this->load->view('petugas/template/topbar', $data);
      $this->load->view('petugas/inputsppdtugas',$data);
      $this->load->view('petugas/template/footer');
  }

  
  public function hapussppd(){
    //jika telah diset URI SEGMENT 3 (id buku) maka hapus data sesuai id yang diset
    //dengan memanggil method model SPPD
    if($this->uri->segment(3)) $this->ModelSurat->delete(array('id_surat'=>$this->uri->segment(3)));
    //kemudian dialihkan ke controller SPPD method index
    redirect('petugas');
  }


}
