-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2019 at 02:06 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_teguh`
--

-- --------------------------------------------------------

--
-- Table structure for table `biaya_sppd`
--

CREATE TABLE `biaya_sppd` (
  `id_biaya_sppd` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `tanggal_biaya` date NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya_sppd`
--

INSERT INTO `biaya_sppd` (`id_biaya_sppd`, `id_surat`, `tanggal_biaya`, `status`) VALUES
(14, 2, '2019-11-25', 1),
(15, 9, '2019-11-25', 1),
(16, 1, '2019-11-25', 1),
(17, 3, '2019-11-25', 1),
(18, 6, '2019-11-25', 1),
(19, 4, '2019-11-25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_biaya_sppd`
--

CREATE TABLE `detail_biaya_sppd` (
  `id_detail_biaya_sppd` int(11) NOT NULL,
  `id_biaya_sppd` int(11) NOT NULL,
  `rincian` varchar(255) NOT NULL,
  `jumlah` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_biaya_sppd`
--

INSERT INTO `detail_biaya_sppd` (`id_detail_biaya_sppd`, `id_biaya_sppd`, `rincian`, `jumlah`) VALUES
(18, 14, 'mandi', 100000),
(19, 15, 'mandi', 100000),
(20, 15, 'mandi', 123),
(21, 16, 'mandi', 100000),
(22, 17, 'mandi', 100000),
(23, 18, 'transportasi', 10000),
(24, 18, 'hotel', 20000),
(25, 19, 'transport', 10000),
(26, 19, 'penginapan', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nip` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `golongan` varchar(50) NOT NULL,
  `jabatan` enum('Kepala Dinas','Sekretaris Dinas','Bendahara Dinas','Petugas Dinas','Pegawai Dinas') NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nip`, `nama`, `golongan`, `jabatan`, `id_user`) VALUES
(1, 100100102, 'Barbara', 'Golongan II', 'Bendahara Dinas', 20),
(21, 12221233, 'Demian', 'Golongan I', 'Kepala Dinas', 21),
(22, 12121212, 'Mamat', 'Golongan I', 'Petugas Dinas', 22),
(24, 1223445, 'Sutisna', 'Golongan I', 'Pegawai Dinas', 24);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id_role`, `nama`) VALUES
(1, 'Super Admin'),
(2, 'Kepala Dinas'),
(3, 'Sekretaris Dinas'),
(4, 'Bendahara Dinas'),
(5, 'Petugas Dinas'),
(6, 'Pegawai Dinas');

-- --------------------------------------------------------

--
-- Table structure for table `sppd`
--

CREATE TABLE `sppd` (
  `id_surat` int(11) NOT NULL,
  `jenissurat` enum('1','2') NOT NULL,
  `nosurat` varchar(20) NOT NULL,
  `tglsurat` date NOT NULL,
  `dasar` varchar(50) NOT NULL,
  `untuk` text NOT NULL,
  `statussurat` enum('1','2','3','4') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sppd`
--

INSERT INTO `sppd` (`id_surat`, `jenissurat`, `nosurat`, `tglsurat`, `dasar`, `untuk`, `statussurat`) VALUES
(1, '1', 'SPPD/2019/001', '2019-10-05', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(2, '2', 'SPPD/2019/002', '2019-10-05', 'makan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(3, '1', 'SPPD/2019/003', '2019-11-21', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(4, '1', 'SPPD/2019/004', '2019-11-21', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(5, '1', 'SPPD/2019/020', '2019-11-26', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(6, '2', 'SPPD/2019/006', '2019-11-21', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(7, '1', 'SPPD/2019/020', '2019-11-26', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(8, '2', 'SPPD/2019/008', '2019-11-21', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(9, '2', 'SPPD/2019/009', '2019-11-21', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(12, '1', 'SPPD/2019/021', '2019-11-26', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(13, '2', 'SPPD/2019/022', '2019-11-26', 'Memenuhi undangan', 'meeting client jakarta, musyawarah nasional, rakornas', '3'),
(15, '1', 'SPPD/2019/023', '2019-11-26', 'SPPD/2019/0123', 'meeting client jakarta, musyawarah nasional, rakornas', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sppd_pegawai`
--

CREATE TABLE `sppd_pegawai` (
  `id_sppdpegawai` int(11) NOT NULL,
  `id_sppd` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sppd_pegawai`
--

INSERT INTO `sppd_pegawai` (`id_sppdpegawai`, `id_sppd`, `id_pegawai`) VALUES
(12, 5, 1),
(13, 5, 21),
(14, 7, 1),
(15, 7, 21),
(19, 12, 1),
(20, 12, 22),
(21, 13, 1),
(22, 13, 21),
(23, 14, 1),
(24, 14, 21),
(25, 14, 22),
(26, 15, 1),
(27, 15, 21),
(28, 15, 22);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `id_role`) VALUES
(1, 'Superadmin', 'superadmin', '123456', 1),
(20, 'Barbara', 'bendahara', '123456', 4),
(21, 'Demian', 'kadis', '123456', 2),
(22, 'Mamat', 'petugas', '123456', 5),
(24, 'Sutisna', 'pegawai', '123456', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biaya_sppd`
--
ALTER TABLE `biaya_sppd`
  ADD PRIMARY KEY (`id_biaya_sppd`),
  ADD KEY `id_surat` (`id_surat`);

--
-- Indexes for table `detail_biaya_sppd`
--
ALTER TABLE `detail_biaya_sppd`
  ADD PRIMARY KEY (`id_detail_biaya_sppd`),
  ADD KEY `detail_biaya_sppd_id_detail_biaya_foreign` (`id_biaya_sppd`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `sppd`
--
ALTER TABLE `sppd`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `sppd_pegawai`
--
ALTER TABLE `sppd_pegawai`
  ADD PRIMARY KEY (`id_sppdpegawai`),
  ADD KEY `id_sppd` (`id_sppd`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_role` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biaya_sppd`
--
ALTER TABLE `biaya_sppd`
  MODIFY `id_biaya_sppd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `detail_biaya_sppd`
--
ALTER TABLE `detail_biaya_sppd`
  MODIFY `id_detail_biaya_sppd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sppd`
--
ALTER TABLE `sppd`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sppd_pegawai`
--
ALTER TABLE `sppd_pegawai`
  MODIFY `id_sppdpegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `biaya_sppd`
--
ALTER TABLE `biaya_sppd`
  ADD CONSTRAINT `biaya_sppd_ibfk_1` FOREIGN KEY (`id_surat`) REFERENCES `sppd` (`id_surat`);

--
-- Constraints for table `detail_biaya_sppd`
--
ALTER TABLE `detail_biaya_sppd`
  ADD CONSTRAINT `detail_biaya_sppd_id_detail_biaya_foreign` FOREIGN KEY (`id_biaya_sppd`) REFERENCES `biaya_sppd` (`id_biaya_sppd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
